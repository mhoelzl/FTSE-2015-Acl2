(in-package "ACL2")

;;; ============================================================
;;; Material from Previous Lectures
;;; ============================================================
(include-book "prop-logic")

;;; Removing implication-like subterms
;;; ==================================

(defun remove-implications (exp)
  (declare (xargs :guard (exprp exp)))
  (cond ((atom exp)
         (if (symbolp exp)
           exp
           nil))
        ((and (true-listp exp) (equal (length exp) 2))
         (if (equal (first exp) 'not)
           `(not ,(remove-implications (second exp)))
           nil))
        ((and (true-listp exp) (equal (length exp) 3))
         (if (member (first exp) *operators*)
           (let ((lhs (remove-implications (second exp)))
                 (rhs (remove-implications (third exp))))
             (case (first exp)
                   (or       `(or ,lhs ,rhs))
                   (and      `(and ,lhs ,rhs))
                   (implies  `(or (not ,lhs) ,rhs))
                   (iff      `(or (and (not ,lhs) (not ,rhs))
                                  (and ,lhs ,rhs)))
                   (xor      `(or (and ,lhs (not ,rhs))
                                  (and (not ,lhs) ,rhs)))
                   (otherwise nil)))
           nil))
        (t nil)))

(thm (and (equal (remove-implications '(implies (and a b) c))
                 '(or (not (and a b)) c))
          (equal (remove-implications '(and (implies a b) (xor (iff a b) c)))
                 '(and (or (not a) b)
                       (or (and (or (and (not a) (not b)) (and a b))
                                (not c))
                           (and (not (or (and (not a) (not b)) (and a b)))
                                c))))))

(defthm removing-implications-preserves-expressions
  (implies (exprp exp)
           (exprp (remove-implications exp)))
  :rule-classes :type-prescription)
  
(defthmd removing-implications-preserves-meaning
  (implies (exprp exp)
           (equal (eval-bool exp env) 
                  (eval-bool (remove-implications exp) env))))

(defun implication-free-exprp (exp)
  (declare (xargs :guard t))
  (cond ((atom exp)
         (symbolp exp))
        ((and (true-listp exp) (equal (length exp) 2))
         (and (equal (first exp) 'not)
              (implication-free-exprp (second exp))))
        ((and (true-listp exp) (equal (length exp) 3))
         (case (first exp)
               ((or and)
                (and
                 (implication-free-exprp (second exp))
                 (implication-free-exprp (third exp))))
               (otherwise
                nil)))
        (t nil)))

(thm (and (implication-free-exprp
           (remove-implications '(implies (and a b) c)))
          (implication-free-exprp
           (remove-implications '(and (implies a b) (xor (iff a b) c))))))

(defthm removing-implications-produces-implication-free-expr
  (implies (exprp exp)
           (implication-free-exprp (remove-implications exp)))
  :rule-classes :type-prescription)

;;; Convert into Negation Normal Form

(defun should-not-happen-fun (value term fun)
  (declare (xargs :guard t))
  (prog2$
   (cw "Should not happen! Reached term ~x0 in function ~x1.~%"
       term fun)
   value))

(defmacro should-not-happen (term &optional (fun '<unknown>))
  `(should-not-happen-fun ,term ',term ',fun))

(defun push-negations-inward (exp)
  (declare (xargs :guard (implication-free-exprp exp)))
  (cond (;; Exp is atom, thus not negated.  Return it as is.
         (atom exp)
         exp)
        (;; Exp is list of length 2 thus potentially a negation
         (and (true-listp exp) (equal (length exp) 2))
         (if (equal (first exp) 'not)
           ;; Yup, we have found a negation.
           (cond (;; If the negated element is an atom we have found
                  ;; a literal.
                  (atom (second exp))
                  exp)
                 (;; If we have a double negation, just remove it.
                  (equal (first (second exp)) 'not)
                  (push-negations-inward (second (second exp))))
                 (;; We have found a term of the form (not (or ...))
                  ;; or (not (and ...)). Convert it to 
                  ;; (and (not ...) ...) or (or (not ...) ...),
                  ;; respectively.
                  (equal (length (second exp)) 3)
                  (let* ((subexp (second exp))
                         (op (first subexp))
                         (lhs (push-negations-inward
                               `(not ,(second subexp))))
                         (rhs (push-negations-inward
                               `(not ,(third subexp)))))
                    (case op
                          (or `(and ,lhs ,rhs))
                          (and `(or ,lhs ,rhs))
                          (otherwise 
                           (should-not-happen exp push-negations-inward)))))
                 (t (should-not-happen exp push-negations-inward)))
           (should-not-happen exp push-negations-inward)))
        (;; A true list of length three, must be (and ...) or (or ...)
         (and (true-listp exp) (equal (length exp) 3))
         `(,(first exp)
           ,(push-negations-inward (second exp))
           ,(push-negations-inward (third exp))))
        (t (should-not-happen exp push-negations-inward))))

(defun nnf-exprp (exp)
  (declare (xargs :guard t))
  (cond ((atom exp)
         (symbolp exp))
        ((and (true-listp exp) (equal (length exp) 2))
         (and (equal (first exp) 'not)
              (symbolp (second exp))))
        ((and (true-listp exp) (equal (length exp) 3))
         (case (first exp)
               ((or and)
                (and
                 (nnf-exprp (second exp))
                 (nnf-exprp (third exp))))
               (otherwise
                nil)))
        (t nil)))

(defthm nnf-expr-characterization
  (iff (nnf-exprp exp)
       (or (symbolp exp)
           (and (true-listp exp) (equal (length exp) 2)
                (equal (first exp) 'not)
                (symbolp (second exp)))
           (and (true-listp exp) (equal (length exp) 3)
                (or (equal (first exp) 'or)
                    (equal (first exp) 'and))
                (nnf-exprp (second exp))
                (nnf-exprp (third exp)))))
  :rule-classes nil)

(defun nnf-expr-listp (xs)
  (declare (xargs :guard t))
  (if (atom xs)
    (null xs)
    (and (nnf-exprp (first xs))
         (nnf-expr-listp (rest xs)))))

(defthm push-negations-inward-generates-nnf
  (implies (implication-free-exprp exp)
           (nnf-exprp (push-negations-inward exp)))
  :rule-classes :type-prescription)

(defthm push-negations-inward-preserves-meaning
  (implies (implication-free-exprp exp)
           (equal (eval-bool exp env) 
                  (eval-bool (push-negations-inward exp) env)))
  :rule-classes nil)#|ACL2s-ToDo-Line|#


(defun distribute-or-inward (exp)
  (declare (xargs :mode :program))
  (cond (;; We have a positive literal
         (atom exp)
         exp)
        (;; We have a negative literal, since `exp' is in NNF
         (and (true-listp exp) (equal (length exp) 2))
         exp)
        (;; We have a term of the form (and ...) or (or ...)
         (and (true-listp exp) (equal (length exp) 3))
         (let* ((op (first exp))
                (lhs (distribute-or-inward (second exp)))
                (lhs-op (and (consp lhs) (first lhs)))
                (rhs (distribute-or-inward (third exp)))
                (rhs-op (and (consp rhs) (first rhs))))
           (case op
               (and
                `(and ,(distribute-or-inward lhs)
                      ,(distribute-or-inward rhs)))
               (or
                (cond ((equal lhs-op 'and)
                       `(and ,(distribute-or-inward `(or ,(second lhs) ,rhs))
                             ,(distribute-or-inward `(or ,(third lhs) ,rhs))))
                      ((equal rhs-op 'and)
                       `(and ,(distribute-or-inward `(or ,lhs ,(second rhs)))
                             ,(distribute-or-inward `(or ,lhs ,(third rhs)))))
                      (t
                       `(or ,lhs ,rhs))))
               (otherwise (should-not-happen exp distribute-or-inward)))))
        (t
         (should-not-happen exp distribute-or-inward))))

(defun cnf (exp)
  (declare (xargs :mode :program))
  (distribute-or-inward
   (push-negations-inward
    (remove-implications exp))))

(cnf '(iff phi (or psi xi)))