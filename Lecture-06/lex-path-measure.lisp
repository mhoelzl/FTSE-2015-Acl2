; ****************** BEGIN INITIALIZATION FOR ACL2s MODE ****************** ;
; (Nothing to see here!  Your actual file is after this initialization code);

#+acl2s-startup (er-progn (assign fmt-error-msg "Problem loading the CCG book.~%Please choose \"Recertify ACL2s system books\" under the ACL2s menu and retry after successful recertification.") (value :invisible))
(include-book "ccg/ccg" :uncertified-okp nil :dir :acl2s-modes :ttags ((:ccg)) :load-compiled-file nil);v4.0 change

;Common base theory for all modes.
#+acl2s-startup (er-progn (assign fmt-error-msg "Problem loading ACL2s base theory book.~%Please choose \"Recertify ACL2s system books\" under the ACL2s menu and retry after successful recertification.") (value :invisible))
(include-book "base-theory" :dir :acl2s-modes)


#+acl2s-startup (er-progn (assign fmt-error-msg "Problem loading ACL2s customizations book.~%Please choose \"Recertify ACL2s system books\" under the ACL2s menu and retry after successful recertification.") (value :invisible))
(include-book "custom" :dir :acl2s-modes :uncertified-okp nil :ttags :all)

#+acl2s-startup (er-progn (assign fmt-error-msg "Problem setting up ACL2s mode.") (value :invisible))

;Settings common to all ACL2s modes
(acl2s-common-settings)
;(acl2::xdoc acl2s::defunc) ;; 3 seconds is too much time to spare -- commenting out [2015-02-01 Sun]

; Non-events:
;(set-guard-checking :none)

(acl2::in-package "ACL2S")

; ******************* END INITIALIZATION FOR ACL2s MODE ******************* ;
;$ACL2s-SMode$;ACL2s
(in-package "ACL2")

(defun max-path-length (x)
  (declare (xargs :guard t))
  (if (atom x)
    0
    (1+ (max (max-path-length (car x))
             (max-path-length (cdr x))))))

(defun lex-path-measurep (x)
  (declare (xargs :guard t))
  (if (atom x)
    (null x)
    (let ((head (first x)))
      (and (consp head)
           (natp (car head))
           (integerp (cdr head)) (< 0 (cdr head))
           (lex-path-measurep (rest x))
           (implies (consp (rest x))
                    (and ;(consp (first (rest x)))
                         (natp (car (first (rest x))))
                         (> (car head) (car (first (rest x))))))))))

(thm
 (lex-path-measurep '((7 . 1) (5 . 3) (2 . 1) (1 . 4))))

(thm
 (not (lex-path-measurep '((7 . 1) (5 . 3) (6 . 1) (1 . 4)))))

(defun update-alist (key new-value alist)
  (if (endp alist)
    (list (cons key new-value))
    (if (equal (car (first alist)) key)
      (cons (cons key new-value) (rest alist))
      (cons (first alist)
            (update-alist key new-value (rest alist))))))

(defun lp-sum (lhs rhs)
  (declare (xargs :measure (length lhs)))
  (if (endp lhs)
    rhs
    (let* ((lhs-pair (first lhs))
           (lhs-key (car lhs-pair))
           (lhs-value (cdr lhs-pair))
           (rhs-pair (assoc-equal lhs-key rhs))
           (rhs-value (if rhs-pair (cdr rhs-pair) 0)))
      (lp-sum (rest lhs)
              (update-alist lhs-key
                            (+ lhs-value rhs-value)
                            rhs)))))

(lp-sum '((7 . 1) (5 . 3) (2 . 1) (1 . 4))
        '((7 . 2) (4 . 7) (2 . 1)))

(defthm lp-sum-for-single-elt-list-lhs/1
  (implies (not (equal key1 key2))
           (equal (assoc-equal key1 (lp-sum `((,key2 . ,k)) alist))
                  (assoc-equal key1 alist))))

(defthm lp-sum-for-single-elt-list-lhs/2
  (implies (and (acl2-numberp k)
                (acl2-numberp (cdr (assoc-equal key alist))))
           (equal (cdr (assoc-equal key (lp-sum `((,key . ,k)) alist)))
                  (+ k (cdr (assoc-equal key alist))))))

(defun no-duplicates-equal (list)
  (if (endp list)
    t
    (and (not (member (first list) (rest list)))
         (no-duplicates-equal (rest list)))))

(thm (no-duplicates-equal '(a b c d 1 2 3)))
(thm (not (no-duplicates-equal '(a b c d 1 2 3 b))))

(defun no-duplicate-keysp (alist)
  (no-duplicates-equal (strip-cars alist)))
(thm (no-duplicate-keysp '((a . 1) (b . 2) (1 . c))))
(thm (not (no-duplicate-keysp '((a . 1) (b . 2) (a . 3)))))#|ACL2s-ToDo-Line|#


(defthm lp-sum-for-single-elt-list-rhs/1
  (implies (and (alistp alist) (not (equal key1 key2)))
           (equal (assoc-equal key1 (lp-sum alist `((,key2 . ,k))))
                  (assoc-equal key1 alist))))

(defthm lp-sum-for-single-elt-list-rhs/2
  (implies (and (acl2-numberp k)
                (acl2-numberp (cdr (assoc key alist))))
           (equal (cdr (assoc key (lp-sum alist `((,key . ,k)))))
                  (+ k (cdr (assoc key alist))))))

(defun lp-inc (key alist)
  (lp-sum `((,key . 1)) alist))

(defthm lp-inc-does-not-change-other-indices
  (implies (not (equal key1 key2))
           (equal (assoc key1 (lp-inc key2 alist))
                  (assoc key1 alist))))

(defthm lp-inc-increments-index
  (implies (acl2-numberp (cdr (assoc key alist)))
           (equal (cdr (assoc key (lp-inc key alist)))
                  (1+ (cdr (assoc key alist))))))

(defthm associativity-of-lp-sum/lemma-1
  (implies (and (lex-path-measurep m1)
                (lex-path-measurep m2)
                (consp m1) (consp m2)
                (equal (caar m1) (caar m2)))
           (equal (lp-sum m1 m2)
                  (cons (cons (caar m1) (+ (cdar m1) (cdar m2)))
                        (lp-sum (rest m1) (rest m2))))))
 
(defthm associativity-of-lp-sum/lemma-2
  (implies (and (lex-path-measurep m1)
                (lex-path-measurep m2)
                (lex-path-measurep m3)
                (consp m1) (consp m2)
                (equal (caar m1) (caar m2)))
           (equal (lp-sum m1 m2)
                  (cons (cons (caar m1) (+ (cdar m1) (cdar m2)))
                        (lp-sum (rest m1) (rest m2))))))

  ;:hints (("Goal"
  ;         :use ((:instance lp-sum 
  ;                (lhs (list (cons l1 v1)))
  ;                (rhs (lp-sum (list (cons l1 v2)) m3)))))))
            

(defthm associativity-of-lp-sum
  (equal (lp-sum (lp-sum m1 m2) m3)
         (lp-sum m1 (lp-sum m2 m3))))

(defun lp< (lhs rhs)
  (declare (xargs :guard (and (lex-path-measurep lhs)
                              (lex-path-measurep rhs))))
  (cond ((endp rhs) nil)
        ((endp lhs) t)
        (t (let* ((lhs-head (first lhs))
                  (lhs-max (car lhs-head))
                  (lhs-count (cdr lhs-head))
                  (rhs-head (first rhs))
                  (rhs-max (car rhs-head))
                  (rhs-count (cdr rhs-head)))
             (cond ((< lhs-max rhs-max) t)
                   ((< rhs-max lhs-max) nil)
                   ((< lhs-count rhs-count) t)
                   ((< rhs-count lhs-count) nil)
                   (t (lp< (rest lhs) (rest rhs))))))))

(thm (not (lp< '((7 . 1) (5 . 3) (2 . 1) (1 . 4))
               '((7 . 1) (5 . 3) (2 . 1) (1 . 4)))))

(thm (lp< '((7 . 1) (5 . 3) (2 . 1) (1 . 4))
          '((7 . 1) (5 . 4) (2 . 1) (1 . 4))))

(thm (lp< '((7 . 1) (5 . 3) (2 . 1) (1 . 4))
          '((7 . 1) (6 . 3) (2 . 1) (1 . 4))))

(defthm lp<-not-reflexive
  (implies (lex-path-measurep lhs)
           (not (lp< lhs lhs))))

(defthm lp<-lp-sum-lhs
  (implies (and (lex-path-measurep lhs) (lex-path-measurep rhs))
           (iff (lp< lhs (lp-sum lhs rhs))
                (not (null rhs)))))

(defthm lp<-lp-sum-rhs
  (implies (and (lex-path-measurep lhs) (lex-path-measurep rhs))
           (iff (lp< rhs (lp-sum lhs rhs))
                (not (null lhs)))))

(defun lp<= (lhs rhs)
  (declare (xargs :guard (and (lex-path-measurep lhs)
                              (lex-path-measurep rhs))))
  (or (equal lhs rhs)
      (lp< lhs rhs)))

(defun or-tree-measure (exp)
  (declare (xargs :guard (nnf-exprp exp)))
  (cond ((atom exp)
         '())
        ((equal (first exp) 'or)
         (lp-inc (max-path-length exp)
                 (lp-sum (or-tree-measure (second exp))
                         (or-tree-measure (third exp)))))
        ((and (true-listp exp)
              (equal (length exp) 2)
              (equal (first exp) 'not))
         (or-tree-measure (second exp)))
        ((and (true-listp exp)
              (equal (length exp) 3)
              (equal (first exp) 'and))
         (lp-sum (or-tree-measure (second exp))
                 (or-tree-measure (third exp))))
         (t
          '())))
