; ****************** BEGIN INITIALIZATION FOR ACL2s MODE ****************** ;
; (Nothing to see here!  Your actual file is after this initialization code);

#+acl2s-startup (er-progn (assign fmt-error-msg "Problem loading the CCG book.~%Please choose \"Recertify ACL2s system books\" under the ACL2s menu and retry after successful recertification.") (value :invisible))
(include-book "ccg/ccg" :uncertified-okp nil :dir :acl2s-modes :ttags ((:ccg)) :load-compiled-file nil);v4.0 change

;Common base theory for all modes.
#+acl2s-startup (er-progn (assign fmt-error-msg "Problem loading ACL2s base theory book.~%Please choose \"Recertify ACL2s system books\" under the ACL2s menu and retry after successful recertification.") (value :invisible))
(include-book "base-theory" :dir :acl2s-modes)


#+acl2s-startup (er-progn (assign fmt-error-msg "Problem loading ACL2s customizations book.~%Please choose \"Recertify ACL2s system books\" under the ACL2s menu and retry after successful recertification.") (value :invisible))
(include-book "custom" :dir :acl2s-modes :uncertified-okp nil :ttags :all)

#+acl2s-startup (er-progn (assign fmt-error-msg "Problem setting up ACL2s mode.") (value :invisible))

;Settings common to all ACL2s modes
(acl2s-common-settings)
;(acl2::xdoc acl2s::defunc) ;; 3 seconds is too much time to spare -- commenting out [2015-02-01 Sun]

; Non-events:
;(set-guard-checking :none)

(acl2::in-package "ACL2S")

; ******************* END INITIALIZATION FOR ACL2s MODE ******************* ;
;$ACL2s-SMode$;ACL2s

(defun distribute-toplevel-or-inward (exp)
  (declare (xargs :guard (nnf-exprp exp)))
  (if (and (true-listp exp) (equal (length exp) 3)
           (equal (first exp) 'or))
    (let* ((lhs (second exp))
           (lhs-op (and (consp lhs) (first lhs)))
           (rhs (third exp))
           (rhs-op (and (consp rhs) (first rhs))))
      (cond ((and (equal lhs-op 'and) (equal rhs-op 'and))
             `(and (and (or ,(second lhs) ,(second rhs))
                        (or ,(third lhs) ,(second rhs)))
                   (and (or ,(second lhs) ,(third rhs))
                        (or ,(third lhs) ,(third rhs)))))
            ((equal lhs-op 'and)
             `(and (or ,(second lhs) ,rhs)
                   (or ,(third lhs) ,rhs)))
            ((equal rhs-op 'and)
             `(and (or ,lhs ,(second rhs))
                   (or ,lhs ,(third rhs))))
            (t
             `(or ,lhs ,rhs))))
    exp))

(defthm distribute-toplevel-or-inward-preserves-nnf
  (implies (nnf-exprp exp)
           (nnf-exprp (distribute-toplevel-or-inward exp)))
  :rule-classes :type-prescription)

(defthm distribute-toplevel-or-inward-preserves-meaning
  (implies (nnf-exprp exp)
           (equal (eval-bool exp env) 
                  (eval-bool (distribute-toplevel-or-inward exp) env)))
  :rule-classes nil)

(defun max-path-length (x)
  (declare (xargs :guard t))
  (if (atom x)
    0
    (1+ (max (max-path-length (car x))
             (max-path-length (cdr x))))))

(defun lex-path-measurep (x)
  (declare (xargs :guard t))
  (if (atom x)
    (null x)
    (let ((head (first x)))
      (and (consp head)
           (natp (car head))
           (integerp (cdr head)) (< 0 (cdr head))
           (lex-path-measurep (rest x))
           (implies (consp (rest x))
                    (and ;(consp (first (rest x)))
                         (natp (car (first (rest x))))
                         (> (car head) (car (first (rest x))))))))))

(thm
 (lex-path-measurep '((7 . 1) (5 . 3) (2 . 1) (1 . 4))))

(thm
 (not (lex-path-measurep '((7 . 1) (5 . 3) (6 . 1) (1 . 4)))))

(defun lp-inc (length measure)
  (declare (xargs :guard (lex-path-measurep measure)))
  (cond
   ((not (natp length))
    (if (lex-path-measurep measure)
      measure
      '()))
   ((or (endp measure) (not (lex-path-measurep measure)))
    (list (cons length 1)))
   (t
    (let* ((head (first measure))
           (head-length (car head)))
      (cond ((equal head-length length)
             (cons (cons head-length (1+ (cdr head)))
                   (rest measure)))
            ((< head-length length)
             (cons (cons length 1)
                   measure))
            (t (cons head 
                     (lp-inc length (rest measure)))))))))

(defthm lp-inc-preserves-lex-path-measurep
  (lex-path-measurep (lp-inc length measure))
  :rule-classes :type-prescription)

(defun lp-sum (lhs rhs)
  (declare (xargs :guard (and (lex-path-measurep lhs)
                              (lex-path-measurep rhs))
                  :measure (+ (length lhs) (length rhs))))                  
  (cond ((or (endp lhs) (not (lex-path-measurep lhs)))
         (if (lex-path-measurep rhs)
           rhs
           '()))
        ((or (endp rhs) (not (lex-path-measurep rhs))) lhs)
        (t (let* ((lhs-head (first lhs))
                  (lhs-max (car lhs-head))
                  (rhs-head (first rhs))
                  (rhs-max (car rhs-head)))
             (cond ((< lhs-max rhs-max)
                    (cons rhs-head
                          (lp-sum lhs (rest rhs))))
                   ((< rhs-max lhs-max)
                    (cons lhs-head
                          (lp-sum (rest lhs) rhs)))
                   (t
                    (cons (cons lhs-max
                                (+ (cdr lhs-head) (cdr rhs-head)))
                          (lp-sum (rest lhs) (rest rhs)))))))))

(defthm lp-sum-preserves-lex-path-measurep
  (lex-path-measurep (lp-sum lhs rhs))
  :rule-classes :type-prescription)

(lp-sum '((7 . 1) (5 . 3) (2 . 1) (1 . 4))
        '((7 . 2) (4 . 7) (2 . 1)))

(defthm lp-inc-lp-sum
  (equal (lp-inc length measure)
         (lp-sum (list (cons length 1)) measure)))

(defthm associativity-of-lp-sum/lemma-1
  (implies (and (lex-path-measurep m1)
                (lex-path-measurep m2)
                (consp m1) (consp m2)
                (equal (caar m1) (caar m2)))
           (equal (lp-sum m1 m2)
                  (cons (cons (caar m1) (+ (cdar m1) (cdar m2)))
                        (lp-sum (rest m1) (rest m2))))))
 
(defthm associativity-of-lp-sum/lemma-2
  (implies (and (lex-path-measurep m1)
                (lex-path-measurep m2)
                (lex-path-measurep m3)
                (consp m1) (consp m2)
                (equal (caar m1) (caar m2)))
           (equal (lp-sum m1 m2)
                  (cons (cons (caar m1) (+ (cdar m1) (cdar m2)))
                        (lp-sum (rest m1) (rest m2))))))

  ;:hints (("Goal"
  ;         :use ((:instance lp-sum 
  ;                (lhs (list (cons l1 v1)))
  ;                (rhs (lp-sum (list (cons l1 v2)) m3)))))))
            

(defthm associativity-of-lp-sum
  (equal (lp-sum (lp-sum m1 m2) m3)
         (lp-sum m1 (lp-sum m2 m3))))

(defun lp< (lhs rhs)
  (declare (xargs :guard (and (lex-path-measurep lhs)
                              (lex-path-measurep rhs))))
  (cond ((endp rhs) nil)
        ((endp lhs) t)
        (t (let* ((lhs-head (first lhs))
                  (lhs-max (car lhs-head))
                  (lhs-count (cdr lhs-head))
                  (rhs-head (first rhs))
                  (rhs-max (car rhs-head))
                  (rhs-count (cdr rhs-head)))
             (cond ((< lhs-max rhs-max) t)
                   ((< rhs-max lhs-max) nil)
                   ((< lhs-count rhs-count) t)
                   ((< rhs-count lhs-count) nil)
                   (t (lp< (rest lhs) (rest rhs))))))))

(thm (not (lp< '((7 . 1) (5 . 3) (2 . 1) (1 . 4))
               '((7 . 1) (5 . 3) (2 . 1) (1 . 4)))))

(thm (lp< '((7 . 1) (5 . 3) (2 . 1) (1 . 4))
          '((7 . 1) (5 . 4) (2 . 1) (1 . 4))))

(thm (lp< '((7 . 1) (5 . 3) (2 . 1) (1 . 4))
          '((7 . 1) (6 . 3) (2 . 1) (1 . 4))))

(defthm lp<-not-reflexive
  (implies (lex-path-measurep lhs)
           (not (lp< lhs lhs))))

(defthm lp<-lp-sum-lhs
  (implies (and (lex-path-measurep lhs) (lex-path-measurep rhs))
           (iff (lp< lhs (lp-sum lhs rhs))
                (not (null rhs)))))

(defthm lp<-lp-sum-rhs
  (implies (and (lex-path-measurep lhs) (lex-path-measurep rhs))
           (iff (lp< rhs (lp-sum lhs rhs))
                (not (null lhs)))))

(defun lp<= (lhs rhs)
  (declare (xargs :guard (and (lex-path-measurep lhs)
                              (lex-path-measurep rhs))))
  (or (equal lhs rhs)
      (lp< lhs rhs)))

(defun or-tree-measure (exp)
  (declare (xargs :guard (nnf-exprp exp)))
  (cond ((atom exp)
         '())
        ((equal (first exp) 'or)
         (lp-inc (max-path-length exp)
                 (lp-sum (or-tree-measure (second exp))
                         (or-tree-measure (third exp)))))
        ((and (true-listp exp)
              (equal (length exp) 2)
              (equal (first exp) 'not))
         (or-tree-measure (second exp)))
        ((and (true-listp exp)
              (equal (length exp) 3)
              (equal (first exp) 'and))
         (lp-sum (or-tree-measure (second exp))
                 (or-tree-measure (third exp))))
         (t
          '())))

(defthm or-tree-measure-is-lex-path-measurep
  (lex-path-measurep (or-tree-measure x))
  :rule-classes :type-prescription)

(defthm or-tree-measure-left-assoc
  (lp< (or-tree-measure `(and (or ,a ,c) (or ,b ,c))) 
       (or-tree-measure `(or (and ,a ,b) ,c))))

(defthm or-tree-measure-right-assoc
  (lp< (or-tree-measure '(and a (or b c)))
       (or-tree-measure '(or (and a b) (and a c)))))

(defthm or-tree-measure-bi-assoc-1
  (lp< (or-tree-measure '(and (and (or a c) (or a d))
                              (and (or b c) (or b d))))
       (or-tree-measure '(or (and a b) (and c d)))))

(defthm or-tree-measure-bi-assoc-2
  (lp< (or-tree-measure '(and (and (or a c) (or b c))
                              (and (or a d) (or b d))))
       (or-tree-measure '(or (and a b) (and c d)))))


(defthm distribute-toplevel-or-inward-decreases-or-tree-depth
  (implies (nnf-exprp exp)
           (lp<= (or-tree-measure (distribute-toplevel-or-inward exp))
                 (or-tree-measure exp)))
  :rule-classes nil)

#+(or)
(defun distribute-or-inward (exp)
  (let ((res (distribute-or-inward-aux exp)))
    (if (equal exp res)
      res
      (distribute-or-inward res))))