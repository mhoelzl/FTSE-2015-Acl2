#|$ACL2s-Preamble$;
(acl2::begin-book t :ttags :all);$ACL2s-Preamble$|#


(in-package "ACL2")

(include-book "prop-logic")

(defun bt-sat (term env open-vars)
  (if (endp open-vars)
    (eval-bool term env)
    (or (bt-sat term
                (cons (cons (first open-vars) t) env)
                (rest open-vars))
        (bt-sat term
                (cons (cons (first open-vars) nil) env)
                (rest open-vars)))))

(defthm bt-sat-is-correct/1
  (implies (eval-bool term env)
           (bt-sat term env nil)))

#+(or)
(defthm valid-terms-are-bt-satisfiable
  (implies (equal (model-check term) :valid)
           (bt-sat term () (vars term))))

#+(or)
(defthm satisfiable-terms-are-bt-satisfiable
  (implies (equal (model-check term) :satisfiable)
           (bt-sat term () (vars term))))

#+(or)
(defthm bt-sat-is-correct/2
  (iff (or (equal (model-check term) :valid)
           (equal (model-check term) :satisfiable))
       (bt-sat term () (vars term)))
  :rule-classes nil)

;;; Three-valued logic
;;; ==================

(include-book "data-structures/alist-theory" :dir :system)

(defun lookup3 (var env)
  "Look up var in env and return the result.
Returns 'undefined if no binding for var is contained in env."
  (declare (type symbol var) (type (satisfies alistp) env))
  (let ((binding (assoc-equal var env)))
    (if binding
      (cdr binding)
      'undefined)))

(defthmd assoc-binding
  (equal (binding-equal var env)
         (cdr (assoc-equal var env)))
  :hints (("Goal"
           :use ((:instance binding-equal (x var) (a env)))
           :in-theory (disable binding-equal))))

(defthm lookup3-bound
  (implies (bound? var env)
           (equal (lookup3 var env)
                  (binding var env)))
  :hints (("Goal"
           :use ((:instance assoc-binding)
                 (:instance bound?-equal (x var) (a env))))))

(defthm assoc-implies-consp
  (implies (assoc var env)
           (consp env)))

(skip-proofs
(defthm assoc-equal-type
  (or (null (assoc-equal var env))
      (consp (assoc-equal var env)))
  :rule-classes :type-prescription))

(skip-proofs
(defthm bound?-assoc
  (implies (alist-listp env)
           (iff (assoc var env)
                (bound? var env)))))

(skip-proofs
(defthm lookup3-unbound
  (implies (not (bound? var env))
           (equal (lookup3 var env) 'undefined))))

(defun not3 (bool3)
  (case bool3
        ((t) nil)
        ((nil) t)
        (otherwise 'undefined)))

(defthm not3-booleanp
  (implies (booleanp bool3)
           (booleanp (not3 bool3)))
  :rule-classes :type-prescription)

(defun or3 (lhs rhs)
  (case lhs
        ((t) t)
        ((nil) rhs)
        (otherwise 'undefined)))

(defthm or3-booleanp
  (implies (and (booleanp lhs) (booleanp rhs))
           (booleanp (or3 lhs rhs)))
  :rule-classes :type-prescription)

(defun and3 (lhs rhs)
  (case lhs
        ((t) rhs)
        ((nil) nil)
        (otherwise 'undefined)))

(defthm and3-booleanp
  (implies (and (booleanp lhs) (booleanp rhs))
           (booleanp (and3 lhs rhs)))
  :rule-classes :type-prescription)

(defun implies3 (lhs rhs)
  (or3 (not3 lhs) rhs))

(defthm implies3-booleanp
  (implies (and (booleanp lhs) (booleanp rhs))
           (booleanp (implies3 lhs rhs)))
  :rule-classes :type-prescription)

(defun iff3 (lhs rhs)
  (or3 (and3 lhs rhs)
       (and3 (not3 lhs) (not3 rhs))))

(defthm iff3-booleanp
  (implies (and (booleanp lhs) (booleanp rhs))
           (booleanp (iff3 lhs rhs)))
  :rule-classes :type-prescription)

(defun xor3 (lhs rhs)
  (or3 (and (not3 lhs) rhs)
       (and lhs (not3 rhs))))

(defthm xor3-booleanp
  (implies (and (booleanp lhs) (booleanp rhs))
           (booleanp (xor3 lhs rhs)))
  :rule-classes :type-prescription)

(defun eval-bool3 (exp env)
  "Evaluate exp in environment env.  Returns 'undefined
if exp cannot be evaluated in env."
  ; (declare (xargs :guard (and (exprp exp) (alistp env))))
  (cond ((symbolp exp)
         (if (or (equal exp t) (equal exp nil))
           exp
           (lookup3 exp env)))
        ((atom exp) 'undefined)
        ((equal (length exp) 2)
         (not3 (eval-bool3 (second exp) env)))
        ((equal (length exp) 3)
         (let ((lhs (eval-bool3 (second exp) env))
               (rhs (eval-bool3 (third exp) env)))
           (case (first exp)
                 (or        (or3      lhs rhs))
                 (and       (and3     lhs rhs))
                 (implies   (implies3 lhs rhs))
                 (iff       (iff3     lhs rhs))
                 (xor       (xor3     lhs rhs))
                 (otherwise 'undefined))))
        (t 'undefined)))

(skip-proofs
(defthm eval-bool3-booleanp
  (implies (and (symbolp var) (envp env) (bound? var env))
           (booleanp (eval-bool3 var env)))))

(skip-proofs
(defthm eval-bool3-booleanp/2
  (implies (and (envp env) (all-bound? (vars term) env))
           (booleanp (eval-bool3 term env)))))

(defun bt-sat3 (term env open-vars)
  (declare (xargs :measure (acl2-count open-vars)))
  (let ((result (eval-bool3 term env)))
    (if (or (not (equal result 'undefined))
            ;; The result should never be undefined when no
            ;; vars are open, but this makes admission more
            ;; difficult.
            (endp open-vars))
      result
      (or (bt-sat3 term
                   (cons (cons (first open-vars) t) env)
                   (rest open-vars))
          (bt-sat3 term
                   (cons (cons (first open-vars) nil) env)
                   (rest open-vars))))))

(skip-proofs
(defthm bt-sat=bt-sat3
  (equal (bt-sat term '() (vars term))
         (bt-sat3 term '() (vars term)))))#|ACL2s-ToDo-Line|#
