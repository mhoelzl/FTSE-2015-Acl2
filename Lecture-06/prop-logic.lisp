#|$ACL2s-Preamble$;
; ****************** BEGIN INITIALIZATION FOR ACL2s MODE ****************** ;
; (Nothing to see here!  Your actual file is after this initialization code);

#+acl2s-startup (er-progn (assign fmt-error-msg "Problem loading the CCG book.~%Please choose \"Recertify ACL2s system books\" under the ACL2s menu and retry after successful recertification.") (value :invisible))
(include-book "ccg/ccg" :uncertified-okp nil :dir :acl2s-modes :ttags ((:ccg)) :load-compiled-file nil);v4.0 change

;Common base theory for all modes.
#+acl2s-startup (er-progn (assign fmt-error-msg "Problem loading ACL2s base theory book.~%Please choose \"Recertify ACL2s system books\" under the ACL2s menu and retry after successful recertification.") (value :invisible))
(include-book "base-theory" :dir :acl2s-modes)


#+acl2s-startup (er-progn (assign fmt-error-msg "Problem loading ACL2s customizations book.~%Please choose \"Recertify ACL2s system books\" under the ACL2s menu and retry after successful recertification.") (value :invisible))
(include-book "custom" :dir :acl2s-modes :uncertified-okp nil :ttags :all)

#+acl2s-startup (er-progn (assign fmt-error-msg "Problem setting up ACL2s mode.") (value :invisible))

;Settings common to all ACL2s modes
(acl2s-common-settings)
;(acl2::xdoc acl2s::defunc) ;; 3 seconds is too much time to spare -- commenting out [2015-02-01 Sun]

; Non-events:
;(set-guard-checking :none)

(acl2::in-package "ACL2S")

; ******************* END INITIALIZATION FOR ACL2s MODE ******************* ;
;$ACL2s-SMode$;ACL2s
(acl2::begin-book t :ttags :all);$ACL2s-Preamble$|#


(in-package "ACL2")

;;; Utilities
;;; =========

(defun set-union (xs ys)
  "Compute the union of lists xs and ys regarded as sets."
  (declare (xargs :guard (and (true-listp xs)
                              (true-listp ys))))
  (if (endp xs)
    ys
    (if (member-equal (first xs) ys)
      (set-union (rest xs) ys)
      (cons (first xs) (set-union (rest xs) ys)))))

(defun set-subset (xs ys)
  "Check whether xs is a subset of ys."
  (declare (xargs :guard (and (true-listp xs) (true-listp ys))))
  (if (endp xs)
    t
    (and (member-equal (first xs) ys)
         (set-subset (rest xs) ys))))

;;; Syntax of Propositional Logic
;;; =============================

;;; The available operators
(defconst *operators* '(or and implies iff xor))

(defun varp (exp)
  (declare (xargs :guard t))
  (and (symbolp exp)
       (not (member exp '(t nil)))))

(defun exprp (exp)
  "Check whether exp is an expression."
  (declare (xargs :guard t))
  (cond ((atom exp)
         (symbolp exp))
        ((and (true-listp exp) (equal (length exp) 2))
         (and (equal (first exp) 'not)
              (exprp (second exp))))
        ((and (true-listp exp) (equal (length exp) 3))
         (and (member (first exp) *operators*)
              (exprp (second exp))
              (exprp (third exp))))
        (t nil)))


;;; Interpretation as Booleans
;;; ===========================


(defun envp (alist)
  (if (endp alist)
    (null alist)
    (let ((head (first alist)))
      (and (symbolp (car head))
           (booleanp (cdr head))
           (envp (rest alist))))))

(defun lookup (var env)
  "Look up var in env and return the result as Boolean.
Returns nil if no binding for var is contained in env."
  (declare (type symbol var) (type (satisfies alistp) env))
  (and (cdr (assoc var env)) t))

(defun eval-bool (exp env)
  "Evaluate exp as Boolean value in environment env"
  (declare (xargs :guard (and (exprp exp) (alistp env))))
  (cond ((symbolp exp)
         (if (or (equal exp t) (equal exp nil))
           exp
           (lookup exp env)))
        ((atom exp) nil)
        ((equal (length exp) 2)
         (not (eval-bool (second exp) env)))
        ((equal (length exp) 3)
         (let ((lhs (eval-bool (second exp) env))
               (rhs (eval-bool (third exp) env)))
           (case (first exp)
                 (or        (or      lhs rhs))
                 (and       (and     lhs rhs))
                 (implies   (implies lhs rhs))
                 (iff       (iff     lhs rhs))
                 (xor       (xor     lhs rhs))
                 (otherwise nil))))
        (t nil)))


;;; Substitutions
;;; =============

(defun substitute-1 (new old exp)
  (declare (xargs :guard t))
  (cond ((equal old exp) new)
        ((or (atom exp) (not (true-listp exp))) exp)
        ((equal (length exp) 2)
         `(,(first exp)
           ,(substitute-1 new old (second exp))))
        ((equal (length exp) 3)
         `(,(first exp)
           ,(substitute-1 new old (second exp))
           ,(substitute-1 new old (third exp))))
        (t exp)))

(defthm substitute-1-identity
  (equal (substitute-1 exp1 exp1 exp2)
         exp2))


(defthm substitute-equivalent-expressions
  (implies (equal (eval-bool exp1 env) (eval-bool exp2 env))
           (equal (eval-bool (substitute-1 exp1 exp2 exp3) env)
                  (eval-bool exp3 env))))

(defun substitute-all (new-old-pairs exp)
  (declare (xargs :guard (alistp new-old-pairs)))
  (let ((new-old-pair (assoc exp new-old-pairs :test 'equal)))
    (cond (new-old-pair
           (cdr new-old-pair))
          ((or (atom exp) (not (true-listp exp))) exp)
          ((equal (length exp) 2)
           `(,(first exp)
             ,(substitute-all new-old-pairs (second exp))))
          ((equal (length exp) 3)
           `(,(first exp)
             ,(substitute-all new-old-pairs (second exp))
             ,(substitute-all new-old-pairs (third exp))))
          (t exp))))

;;; Semantic Truth
;;; ==============

(defun alist-listp (list)
  (declare (xargs :guard t))
  (if (atom list)
    (not list)
    (and (alistp (first list))
         (alist-listp (rest list)))))

(defun extend-envs (var value envs)
  (declare (xargs :guard (and (symbolp var) (alist-listp envs))))
  (if (endp envs)
    '()
    (cons (cons (cons var value) (first envs))
          (extend-envs var value (rest envs)))))

(defthm alist-listp-append
  (implies (and (alist-listp xs) (alist-listp ys))
           (alist-listp (append xs ys)))
  :rule-classes :type-prescription)

(defthm extend-envs-alist-listp
  (implies (alist-listp envs)
           (alist-listp (extend-envs var value envs)))
  :rule-classes :type-prescription)

(defun generate-envs (vars)
  (declare (xargs :guard (symbol-listp vars)))
  (if (endp vars)
    '(())
    (let ((rest-envs (generate-envs (rest vars))))
      (append (extend-envs (first vars) t rest-envs)
              (extend-envs (first vars) nil rest-envs)))))

(defthm generate-envs-is-alist-listp
  (alist-listp (generate-envs vars))
  :rule-classes :type-prescription)

(defun vars (exp)
  "Compute all (propositional) variables occurring in exp."
  (declare (xargs :guard (exprp exp)))
  (cond ((atom exp)
         (if (varp exp)
           (list exp)
           '()))
        ((equal (length exp) 2)
         (if (equal (first exp) 'not)
           (vars (second exp))
           '()))
        ((equal (length exp) 3)
         (if (member (first exp) *operators*)
           (set-union (vars (second exp)) (vars (third exp)))
           '()))
        (t '())))

(defun any-true (list)
  (declare (xargs :guard (true-listp list)))
  (if (endp list)
    nil
    (or (and (first list) t)
        (any-true (rest list)))))

(defun all-true (list)
  (declare (xargs :guard (true-listp list)))
  (if (endp list)
    t
    (and (first list)
         (all-true (rest list)))))

(defun model-check-aux (term envs)
  (declare (xargs :guard (and (exprp term) (alist-listp envs))))
  (if (endp envs)
    '()
    (cons (eval-bool term (first envs))
          (model-check-aux term (rest envs)))))

(defun model-check (term)
  (declare (xargs :guard (exprp term)))
  (let ((results (model-check-aux term (generate-envs (vars term)))))
    (cond ((all-true results)
           :valid)
          ((any-true results)
           :satisfiable)
          (t
           :insatisfiable))))#|ACL2s-ToDo-Line|#

