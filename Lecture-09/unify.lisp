; *************** BEGIN INITIALIZATION FOR PROGRAMMING MODE *************** ;
; (Nothing to see here!  Your actual file is after this initialization code);

#+acl2s-startup (er-progn (assign fmt-error-msg "Problem loading the TRACE* book.~%Please choose \"Recertify ACL2s system books\" under the ACL2s menu and retry after successful recertification.") (value :invisible))
; only load for interactive sessions: 
#+acl2s-startup (include-book "trace-star" :uncertified-okp nil :dir :acl2s-modes :ttags ((:acl2s-interaction)) :load-compiled-file nil);v4.0 change

#+acl2s-startup (er-progn (assign fmt-error-msg "Problem loading the EVALABLE-LD-PRINTING book.~%Please choose \"Recertify ACL2s system books\" under the ACL2s menu and retry after successful recertification.") (value :invisible))
; only load for interactive sessions: 
#+acl2s-startup (include-book "hacking/evalable-ld-printing" :uncertified-okp nil :dir :system :ttags ((:evalable-ld-printing)) :load-compiled-file nil);v4.0 change


#+acl2s-startup (er-progn (assign fmt-error-msg "Problem loading ACL2s customizations book.~%Please choose \"Recertify ACL2s system books\" under the ACL2s menu and retry after successful recertification.") (value :invisible))
(include-book "defunc" :dir :acl2s-modes :uncertified-okp nil :load-compiled-file :comp) ;lets add defunc at least harshrc [2015-02-01 Sun]
(include-book "custom" :dir :acl2s-modes :uncertified-okp nil :load-compiled-file :comp)

#+acl2s-startup (er-progn (assign fmt-error-msg "Problem loading programming mode.") (value :invisible))


(er-progn 
  (program)
  (defun book-beginning () ()) ; to prevent book development
  (set-irrelevant-formals-ok :warn)
  (set-bogus-mutual-recursion-ok :warn)
  (set-ignore-ok :warn)
  (set-verify-guards-eagerness 0)
  (set-default-hints '(("Goal" :error "This depends on a proof, and proofs are disabled in Programming mode.  The session mode can be changed under the \"ACL2s\" menu.")))
  (reset-prehistory t)
  (set-guard-checking :none)
  (assign evalable-ld-printingp t)
  (assign evalable-printing-abstractions '(list cons))
  (assign triple-print-prefix "; "))
  

(cw "~@0Programming mode loaded.~%~@1"
      #+acl2s-startup "${NoMoReSnIp}$~%" #-acl2s-startup ""
      #+acl2s-startup "${SnIpMeHeRe}$~%" #-acl2s-startup "")

; **************** END INITIALIZATION FOR PROGRAMMING MODE **************** ;
;$ACL2s-SMode$;Programming
(in-package "ACL2")

(include-book "std/strings/top" :dir :system)

(defun varp (x)
  (and (symbolp x)
       (> (length (symbol-name x)) 1)
       (str::strprefixp "?" (symbol-name x))))

(and (varp '?x)
     (varp '?a-long-variable-name)
     (not (varp '?))
     (not (varp 'x))
     (not (varp 'foo-bar)))

(defun get-binding (x bindings)
  (assoc x bindings))

(defun lookup (x bindings)
  (cdr (get-binding x bindings)))

(defun extend-bindings (var x bindings)
  (cons (cons var x) bindings))

(defun occurs-check (var x bindings)
  (cond ((equal var x) t)
        ((and (varp x) (get-binding x bindings))
         (occurs-check var (lookup x bindings) bindings))
        ((consp x) (or (occurs-check var (first x) bindings)
                       (occurs-check var (rest x) bindings)))
        (t nil)))

(mutual-recursion
 (defun unify* (x y bindings)
   (cond ((not (listp bindings))
          'no-unifier)
         ((equal x y) bindings)
         ((varp x)
          (unify-var x y bindings))
         ((varp y)
          (unify-var y x bindings))
         ((and (consp x) (consp y))
          (unify* (rest x) (rest y)
                  (unify* (first x) (first y) bindings)))
         (t
          'no-unifier)))

 (defun unify-var (var x bindings)
   (cond ((get-binding var bindings)
          (unify* (lookup var bindings) x bindings))
         ((and (varp x) (get-binding x bindings))
          (unify* var (lookup x bindings) bindings))
         ((occurs-check var x bindings)
          'no-unifier)
         (t
          (extend-bindings var x 
                           (subst x var bindings))))))

(defun unify (x y)
  (unify* x y '()))

;;; Simple unifications
(unify '?x '?y)
(unify '(f ?x) '(f ?y))
(unify '(f ?x ?y) '(f ?y ?z))
(unify '(f ?x b) '(f a ?y))
(unify '(f ?x ?x) '(f ?y ?y))
(unify '(f ?x ?x) '(f ?y ?z))
(unify '(f ?y ?z) '(f ?x ?x))

;;; Failing unifications
(unify '(f ?x) '(g ?x))
(unify '(f a) '(f b))
(unify '(f ?x) '(f ?y ?z))


;;; Nested unifications
(unify '(p (f ?x)) '(p ?y))
(unify '(p (f ?x) a) '(p ?y ?x))
(unify '(p ?y ?x) '(p (f ?x) a))
(unify '(f ?x (g (h ?y))) '(f ?z ?z))
(unify '(f ?x (g (h ?y))) '(f (p (q ?z ?y)) ?z))
(unify '(f ?x (g (h ?y))) '(f (p (q ?z)) (g ?w)))

;;; Failing unifications
(unify '(p (f ?x) (f ?y)) '(p ?y ?x))
(unify '(p ?y ?x) '(p (f ?x) (f ?y)))#|ACL2s-ToDo-Line|#

