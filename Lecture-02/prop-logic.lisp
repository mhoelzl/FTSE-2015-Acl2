; ****************** BEGIN INITIALIZATION FOR ACL2s MODE ****************** ;
; (Nothing to see here!  Your actual file is after this initialization code);

#+acl2s-startup (er-progn (assign fmt-error-msg "Problem loading the CCG book.~%Please choose \"Recertify ACL2s system books\" under the ACL2s menu and retry after successful recertification.") (value :invisible))
(include-book "ccg/ccg" :uncertified-okp nil :dir :acl2s-modes :ttags ((:ccg)) :load-compiled-file nil);v4.0 change

;Common base theory for all modes.
#+acl2s-startup (er-progn (assign fmt-error-msg "Problem loading ACL2s base theory book.~%Please choose \"Recertify ACL2s system books\" under the ACL2s menu and retry after successful recertification.") (value :invisible))
(include-book "base-theory" :dir :acl2s-modes)


#+acl2s-startup (er-progn (assign fmt-error-msg "Problem loading ACL2s customizations book.~%Please choose \"Recertify ACL2s system books\" under the ACL2s menu and retry after successful recertification.") (value :invisible))
(include-book "custom" :dir :acl2s-modes :uncertified-okp nil :ttags :all)

#+acl2s-startup (er-progn (assign fmt-error-msg "Problem setting up ACL2s mode.") (value :invisible))

;Settings common to all ACL2s modes
(acl2s-common-settings)
;(acl2::xdoc acl2s::defunc) ;; 3 seconds is too much time to spare -- commenting out [2015-02-01 Sun]

; Non-events:
;(set-guard-checking :none)

(acl2::in-package "ACL2S")

; ******************* END INITIALIZATION FOR ACL2s MODE ******************* ;
;$ACL2s-SMode$;ACL2s
(in-package "ACL2")

;;; Utilities
;;; =========

(defun set-union (xs ys)
  "Compute the union of lists xs and ys regarded as sets."
  (declare (xargs :guard (and (true-listp xs)
                              (true-listp ys))))
  (if (endp xs)
    ys
    (if (member-equal (first xs) ys)
      (set-union (rest xs) ys)
      (cons (first xs) (set-union (rest xs) ys)))))

(defun set-subset (xs ys)
  "Check whether xs is a subset of ys."
  (declare (xargs :guard (and (true-listp xs) (true-listp ys))))
  (if (endp xs)
    t
    (and (member-equal (first xs) ys)
         (set-subset (rest xs) ys))))


;;; Syntax of Propositional Logic
;;; =============================

;;; The available operators
(defconst *operators* '(or and implies iff xor))

(defun varp (exp)
  (declare (xargs :guard t))
  (and (symbolp exp)
       (not (member exp '(t nil)))))

(defun exprp (exp)
  "Check whether exp is an expression."
  (declare (xargs :guard t))
  (cond ((atom exp)
         (symbolp exp))
        ((and (true-listp exp) (equal (length exp) 2))
         (and (equal (first exp) 'not)
              (exprp (second exp))))
        ((and (true-listp exp) (equal (length exp) 3))
         (and (member (first exp) *operators*)
              (exprp (second exp))
              (exprp (third exp))))
        (t nil)))


;;; Some examples

(thm (and (exprp nil)
          (exprp t)
          (exprp 'a)
          (not (exprp 123))
          (not (exprp "a"))))

(thm (and (exprp '(not b))
          (exprp '(and a b))
          (exprp '(or a b))
          (exprp '(implies a b))
          (exprp '(iff a b))
          (exprp '(xor a b))))

(thm (and (exprp '(implies (and (implies a b)
                                (implies b c))
                           (implies a c)))
          (exprp '(implies (implies a b)
                           (implies (not b) (not a))))
          (exprp '(and (or a b)
                       (implies (iff c d)
                                (xor e f))))))

(thm (and (not (exprp '(and)))
          (not (exprp '(and a)))
          (not (exprp '(and a b c)))))

;;; Use predefined functions for association lists (alists).
(include-book "std/alists/top" :dir :system)


;;; Subterms
;;; ========

(defun term<= (lhs rhs)  
  (declare (xargs :guard t
                  :termination-method :measure))
  (if (consp lhs)
    (if (consp rhs)
      (if (and (true-listp lhs) (true-listp rhs))
        (cond ((< (length lhs) (length rhs)) t)
              ((> (length lhs) (length rhs)) nil)
              (t
               (and (term<= (second lhs) (second rhs))
                    (term<= (third lhs) (third rhs)))))
        (term<= (cdr lhs) (cdr rhs)))
      nil)
    (if (consp rhs)
      t
      (if (and (symbolp lhs) (symbolp rhs))
        (and (string<= (symbol-name lhs) (symbol-name rhs)) t)
        t))))
        
(defun insert (x xs)
  (declare (xargs :guard t))
  (cond ((atom xs)
         (list x))
        ((term<= x (first xs))
         (cons x xs))
        (t
         (cons (first xs) (insert x (rest xs))))))

(defun insertion-sort (xs)
  (declare (xargs :guard t))
  (if (atom xs)
    '()
    (insert (first xs)
            (insertion-sort (rest xs)))))

(defun subterms-rec (exp)
  "Return the subterms of exp apart from t and nil."
  (declare (xargs :guard (exprp exp)))
  (cond ((varp exp) (list exp))
        ((atom exp) '())
        ((= (length exp) 2)
         (cons exp (subterms-rec (second exp))))
        ((= (length exp) 3)
         (cons exp 
               (set-union (subterms-rec (second exp))
                          (subterms-rec (third exp)))))
        (t '())))

(defun expr-listp (terms)
  (declare (xargs :guard t))
  (if (atom terms)
    (not terms)
    (and (exprp (first terms))
         (expr-listp (rest terms)))))


(defthm subterms-rec-is-expr-listp
  (implies (exprp exp)
           (expr-listp (subterms-rec exp))))

(defun subterms (exp)
  "Return a sorted list of subterms of exp excluding t and nil."
  (declare (xargs :guard (exprp exp)))
  (insertion-sort (subterms-rec exp)))

(subterms '(or a (not a)))
(subterms '(implies a (not (not a))))
(subterms '(iff a (not (not a))))
(subterms '(iff (implies a b) (implies (not b) (not a))))
(subterms '(implies (and (implies a b) (implies b c))
                    (implies a c)))

(defthmd insert-preserves-expr-listp
  (implies (and (exprp exp) (expr-listp exps))
           (expr-listp (insert exp exps))))

(defthmd insertion-sort-preserves-expr-listp
  (implies (expr-listp exps)
           (expr-listp (insertion-sort exps))))

(defthm subterms-is-expr-listp
  (implies (exprp exp)
           (expr-listp (subterms exp))))


;;; Substitutions
;;; =============

(defun substitute-1 (new old exp)
  (declare (xargs :guard t))
  (cond ((equal old exp) new)
        ((or (atom exp) (not (true-listp exp))) exp)
        ((equal (length exp) 2)
         (list (first exp)
               (substitute-1 new old (second exp))))
        ((equal (length exp) 3)
         (list (first exp)
               (substitute-1 new old (second exp))
               (substitute-1 new old (third exp))))
        (t exp)))

(substitute-1 '(and a b)
              '(and b a)
              '(implies (and b a)
                        (or (and b a) (and c a))))

(defthm substitute-1-identity
  (equal (substitute-1 exp1 exp1 exp2)
         exp2))


;;; Analysis of Terms
;;; =================

(defun vars (exp)
  "Compute all (propositional) variables occurring in exp."
  (declare (xargs :guard (exprp exp)))
  (cond ((atom exp)
         (if (varp exp)
           (list exp)
           '()))
        ((equal (length exp) 2)
         (if (equal (first exp) 'not)
           (vars (second exp))
           '()))
        ((equal (length exp) 3)
         (if (member (first exp) *operators*)
           (set-union (vars (second exp)) (vars (third exp)))
           '()))
        (t '())))


(defun valid-envp (exp env)
  "Check whether env is a valid environment for exp, i.e.,
whether all variables occurring in exp are bound in env." 
  (declare (xargs :guard (and (exprp exp) (alistp env))))
  (set-subset (vars exp) (alist-keys env)))

;;; Interpretation as Booleans
;;; ===========================

(defun lookup (var env)
  "Look up var in env and return the result as Boolean.
Returns nil if no binding for var is contained in env."
  (declare (type symbol var) (type (satisfies alistp) env))
  (and (cdr (assoc var env)) t))

(defun eval-bool (exp env)
  "Evaluate exp as Boolean value in environment env"
  (declare (xargs :guard (and (exprp exp) (alistp env))))
  (cond ((symbolp exp)
         (if (or (equal exp t) (equal exp nil))
           exp
           (lookup exp env)))
        ((atom exp) nil)
        ((equal (length exp) 2)
         (not (eval-bool (second exp) env)))
        ((equal (length exp) 3)
         (let ((lhs (eval-bool (second exp) env))
               (rhs (eval-bool (third exp) env)))
           (case (first exp)
                 (or        (or      lhs rhs))
                 (and       (and     lhs rhs))
                 (implies   (implies lhs rhs))
                 (iff       (iff     lhs rhs))
                 (xor       (xor     lhs rhs))
                 (otherwise nil))))
        (t nil)))

;;; Some theorems to check whether our interpretation as Booleans
;;; is correct.

(defthm eval-bool-t
  (equal (eval-bool t env) t))

(defthm eval-bool-nil
  (equal (eval-bool nil env) nil))

(defthm eval-bool-symbol
  (implies (varp exp)
           (equal (eval-bool exp env) (lookup exp env))))

(defthm eval-bool-atom
  (implies (and (atom exp) (not (symbolp exp)))
           (not (eval-bool exp env))))

(defthm eval-bool-not
  (not (equal (eval-bool (list 'not exp) env) (eval-bool exp env))))

(defthm eval-bool-or
  (equal (eval-bool (list 'or exp1 exp2) env)
         (or (eval-bool exp1 env) (eval-bool exp2 env))))

(defthm eval-bool-and
  (equal (eval-bool (list 'and exp1 exp2) env)
         (and (eval-bool exp1 env) (eval-bool exp2 env))))

(defthm eval-bool-implies
  (equal (eval-bool (list 'implies exp1 exp2) env)
         (or (not (eval-bool exp1 env)) (eval-bool exp2 env))))

(defthm eval-bool-iff
  (equal (eval-bool (list 'iff exp1 exp2) env)
         (or (and (eval-bool exp1 env) (eval-bool exp2 env))
             (and (not (eval-bool exp1 env)) (not (eval-bool exp2 env))))))

(defthm eval-bool-xor
  (equal (eval-bool (list 'xor exp1 exp2) env)
         (or (and (eval-bool exp1 env) (not (eval-bool exp2 env)))
             (and (not (eval-bool exp1 env)) (eval-bool exp2 env)))))


;;; Interpretation as Integers
;;; ==========================

;;; We can not only interpret true and false as Booleans, we
;;; can also interpret them using, e.g., two integers.

;;; No longer needed.
(defun alist-01p (xs)
  "Check whether xs is an alist that binds variables
to 0 and 1 only."
  (declare (xargs :guard t))
  (if (atom xs)
    (not xs)
    (let ((head (first xs)))
      (and (consp head)
           (or (equal (cdr head) 0)
               (equal (cdr head) 1))
           (alist-01p (rest xs))))))

(defun lookup-01 (var env)
  "Look up var in env, returning 1 for true values, 0 for false ones."
  (declare (type symbol var) (type (satisfies alistp) env))
  (or (and (cdr (assoc var env)) 1) 0))

(defthm lookup-01-is-0-or-1
  (or (equal (lookup-01 var env) 0)
      (equal (lookup-01 var env) 1))
  :rule-classes :type-prescription)

;;; Evaluator that maps logical terms into integer values
;;;
(defun eval-int (exp env)
  (declare (xargs :guard (and (exprp exp) (alistp env))
                  :verify-guards nil))
  (cond ((symbolp exp)
         (case exp
               ((nil) 0)
               ((t) 1)
               (otherwise (lookup-01 exp env))))
        ((atom exp) 0)
        ((equal (length exp) 2)
         (if (equal (first exp) 'not)
           (- 1 (eval-int (second exp) env))
           0))
        ((equal (length exp) 3)
         (if (member (first exp) *operators*)
           (let ((lhs (eval-int (second exp) env))
                 (rhs (eval-int (third exp) env)))
             (case (first exp)
                   (or        (if (or (= lhs 1) (= rhs 1))
                                1
                                0))
                   (and       (if (and (= lhs 1) (= rhs 1))
                                1
                                0))
                   (implies   (if (= lhs 0)
                                1
                                (* lhs rhs)))
                   (iff       (if (= lhs rhs)
                                1
                                0))
                   (xor       (if (not (= lhs rhs))
                                1
                                0))
                   (otherwise 0)))
           0))
        (t 0)))

(defthm eval-int-is-0-or-1
  (or (equal (eval-int exp env) 0)
      (equal (eval-int exp env) 1))
  :rule-classes :type-prescription)

(verify-guards eval-int)

(defthm 0-or-1-conditional
  (implies (and (or (= x 0) (= x 1))
                (or (= y 0) (= y 1)))
           (equal (if (or (zp x) (zp y)) 0 1)
                  (* x y)))
  :rule-classes nil)

(defthm eval-int-t
  (= (eval-int t env) 1))

(defthm eval-int-nil
  (= (eval-int nil env) 0))

(defthm eval-int-symbol
  (implies (and (symbolp exp) (not (equal exp t)) (not (equal exp nil)))
           (= (eval-int exp env) (lookup-01 exp env))))

(defthm eval-int-atom
  (implies (and (atom exp) (not (symbolp exp)))
           (= (eval-int exp env) 0)))

(defthm eval-int-not
  (= (eval-int (list 'not exp) env)
         (- 1 (eval-int exp env))))

(defthm eval-int-or
  (equal (= (eval-int (list 'or exp1 exp2) env) 1)
         (or (= (eval-int exp1 env) 1)
             (= (eval-int exp2 env) 1)))
  :rule-classes nil)

(defthm eval-int-and
  (equal (= (eval-int (list 'and exp1 exp2) env) 1)
         (and (= (eval-int exp1 env) 1)
              (= (eval-int exp2 env) 1)))
  :rule-classes nil)

#+(or)
(include-book "arithmetic-5/top" :dir :system)

(defthmd eval-int-and/lemma-1
  (= (eval-int (list 'and exp1 exp2) env)
     (if (and (= (eval-int exp1 env) 1)
              (= (eval-int exp2 env) 1))
       1
       0)))

(defthmd eval-int-and/lemma-2
  (iff (not (= (eval-int exp env) 0))
       (= (eval-int exp env) 1)))

(defthm eval-int-and/lemma-3
  (iff (and (not (= (eval-int exp1 env) 0))
            (not (= (eval-int exp2 env) 0)))
       (and (= (eval-int exp1 env) 1) (= (eval-int exp2 env) 1)))
  :rule-classes nil
  :hints (("Goal"
           :do-not-induct t
           :use ((:instance eval-int-and/lemma-2 (exp exp1) (env env))
                 (:instance eval-int-and/lemma-2 (exp exp2) (env env))))))

(defthmd eval-int-and/lemma-4
  (implies (and (exprp exp1) (exprp exp2))
           (implies (and (= (eval-int exp1 env) 1)
                         (= (eval-int exp2 env) 1))
                    (= (eval-int (list 'and exp1 exp2) env) 1))))

(defthmd eval-int-and/lemma-5
  (implies (and (= (eval-int exp1 env) 1) (= (eval-int exp2 env) 1))
           (= (eval-int (list 'and exp1 exp2) env) 1)))

(defthm eval-int-and/lemma-6a
  (implies (< 0 (eval-int exp env))
           (= 1 (eval-int exp env)))
  :rule-classes nil
  :hints (("Goal"
           :use ((:instance eval-int-and/lemma-2)))))

(defthmd eval-int-and/lemma-6
  (implies (= (eval-int (list 'and exp1 exp2) env) 1)
           (= (eval-int exp1 env) 1)))

(defthmd eval-int-and/lemma-7
  (implies (= (eval-int (list 'and exp1 exp2) env) 1)
           (= (eval-int exp2 env) 1)))

(defthmd eval-int-and/lemma-8
  (implies (or (= (eval-int exp1 env) 0) (= (eval-int exp2 env) 0))
           (= (eval-int (list 'and exp1 exp2) env) 0)))

(defthm eval-int-and/lemma-9
  (implies (= (eval-int (list 'and exp1 exp2) env) 0)
           (or (= (eval-int exp1 env) 0) (= (eval-int exp2 env) 0)))
  :rule-classes nil
  :hints (("Goal"
           :do-not-induct t
           :use ((:instance eval-int-and/lemma-2 (exp exp1) (env env))
                 (:instance eval-int-and/lemma-2 (exp exp2) (env env))))))
  
(defthmd eval-int-and/lemma-10
  (implies (= (eval-int (list 'and exp1 exp2) env) 0)
           (= (eval-int (list 'and exp1 exp2) env) 
              (* (eval-int exp1 env) (eval-int exp2 env))))
  :hints (("Goal"
           :do-not-induct t
           :use ((:instance eval-int-and/lemma-9)))))

(defthmd eval-int-and/lemma-11
  (implies (= (eval-int (list 'and exp1 exp2) env) 1)
           (and (= (eval-int exp1 env) 1) (= (eval-int exp2 env) 1)))
  :hints (("Goal"
           :do-not-induct t
           :use ((:instance eval-int-and/lemma-6)
                 (:instance eval-int-and/lemma-7)))))

(defthmd eval-int-and/lemma-12
  (implies (= (eval-int (list 'and exp1 exp2) env) 1)
           (= (eval-int (list 'and exp1 exp2) env) 
              (* (eval-int exp1 env) (eval-int exp2 env))))
  :hints (("Goal"
           :do-not-induct t
           :use ((:instance eval-int-and/lemma-11)))))

(defthmd eval-int-and-2
  (= (eval-int (list 'and exp1 exp2) env)
     (* (eval-int exp1 env) (eval-int exp2 env)))
  :hints (("Goal"
           :do-not-induct t
           :use ((:instance eval-int-and/lemma-2)
                 (:instance eval-int-and/lemma-10)
                 (:instance eval-int-and/lemma-12)))))

(defthmd eval-int-implies
  (equal (not (= (eval-int (list 'implies exp1 exp2) env) 0))
         (or (= (eval-int exp1 env) 0)
             (not (= (eval-int exp2 env) 0)))))

(defthmd eval-int-iff
  (equal (= (eval-int (list 'iff exp1 exp2) env) 1)
         (= (eval-int exp1 env) (eval-int exp2 env))))


(defthmd eval-int-xor
  (equal (= (eval-int (list 'xor exp1 exp2) env) 1)
         (not (= (eval-int exp1 env) (eval-int exp2 env)))))


;;; Examples From Slides
;;; ====================

(eval-bool '(and a b) '((a . t) (b . t)))
(eval-bool '(and a b) '((a . t) (b . nil)))

(eval-bool '(and a (not a)) '((a . t)))
(eval-bool '(and a (not a)) '((a . nil)))

(eval-bool '(or a (not a)) '((a . t)))
(eval-bool '(or a (not a)) '((a . nil)))

;;; Tautologies
;;; ===========

(defthm a-or-not-a-is-tautology
  (eval-bool '(or a (not a)) env))

#+(or)
(defthm a-and-not-a-is-tautology
  (eval-bool '(and a (not a)) env))

#+(or)
(defthm a-is-tautology
  (eval-bool 'a env))

(defthm a-implies-double-negation
  (eval-bool '(implies a (not (not a))) env))

(defthm a-iff-double-negation
  (eval-bool '(iff a (not (not a))) env))

(defthm contrapositive
  (eval-bool '(iff (implies a b)
                   (implies (not b) (not a)))
             env))

(defthm implies-is-transitive
  (eval-bool '(implies (and (implies a b)
                            (implies b c))
                       (implies a c))
             env))

;;; Substitutions Again
;;; ===================

(defthm substitute-equivalent-expressions
  (implies (equal (eval-bool exp1 env) (eval-bool exp2 env))
           (equal (eval-bool (substitute-1 exp1 exp2 exp3) env)
                  (eval-bool exp3 env))))

(defun substitute-all (new-old-pairs exp)
  (declare (xargs :guard (alistp new-old-pairs)))
  (let ((new-old-pair (assoc exp new-old-pairs :test 'equal)))
    (cond (new-old-pair
           (cdr new-old-pair))
          ((or (atom exp) (not (true-listp exp))) exp)
          ((equal (length exp) 2)
           (list (first exp)
                 (substitute-all new-old-pairs (second exp))))
          ((equal (length exp) 3)
           (list (first exp)
                 (substitute-all new-old-pairs (second exp))
                 (substitute-all new-old-pairs (third exp))))
          (t exp))))

(substitute-all '((a . x))
                '(implies (and b a)
                          (or (and b a) (and c a))))

(substitute-all '(((and b a) . (and a b)))
                '(implies (and b a)
                          (or (and b a) (and c a))))

(substitute-all '(((and b a) . (and a b))
                  ((and c a) . (and a c)))
                '(implies (and b a)
                          (or (and b a) (and c a))))

(substitute-all '(((and b a) . (and a b))
                  (a . x))
                '(implies (and b a)
                          (or (and b a) (and c a))))

(substitute-all '((a . x)
                  ((and b a) . (and a b)))
                '(implies (and b a)
                          (or (and b a) (and c a))))

;;; Truth Tables
;;; ============

(defun eval-terms (terms env)
  (declare (xargs :guard (and (expr-listp terms) (alistp env))))
  (if (endp terms)
    ()
    (cons (eval-bool (first terms) env)
          (eval-terms (rest terms) env))))

(eval-terms (subterms'(implies a (not (not a)))) '((a . nil)))
(eval-terms (subterms'(implies a (not (not a)))) '((a . t)))

(defun alist-listp (list)
  (declare (xargs :guard t))
  (if (atom list)
    (not list)
    (and (alistp (first list))
         (alist-listp (rest list)))))

(defun extend-envs (var value envs)
  (declare (xargs :guard (and (symbolp var) (alist-listp envs))))
  (if (endp envs)
    '()
    (cons (cons (cons var value) (first envs))
          (extend-envs var value (rest envs)))))

(defthm alist-listp-append
  (implies (and (alist-listp xs) (alist-listp ys))
           (alist-listp (append xs ys))))

(defthm extend-envs-alist-listp
  (implies (alist-listp envs)
           (alist-listp (extend-envs var value envs))))

(defun generate-envs (vars)
  (declare (xargs :guard (symbol-listp vars)))
  (if (endp vars)
    '(())
    (let ((rest-envs (generate-envs (rest vars))))
      (append (extend-envs (first vars) t rest-envs)
              (extend-envs (first vars) nil rest-envs)))))

(defthm generate-envs-is-alist-listp
  (alist-listp (generate-envs vars)))

(defun build-truth-table-aux (terms envs)
  (declare (xargs :guard (and (expr-listp terms) (alist-listp envs))))
  (if (endp envs)
    '()
    (cons (eval-terms terms (first envs))
          (build-truth-table-aux terms (rest envs)))))

(defun build-truth-table (term)
  (declare (xargs :guard (exprp term)))
  (cons (subterms term)
        (build-truth-table-aux (subterms term)
                               (generate-envs (vars term)))))

(build-truth-table '(or a (not a)))
(build-truth-table '(implies a (not (not a))))
(build-truth-table '(iff a (not (not a))))
(build-truth-table '(iff (implies a b) (implies (not b) (not a))))
(build-truth-table '(implies (and (implies a b) (implies b c))
                             (implies a c)))

(defun any-true (list)
  (declare (xargs :guard (true-listp list)))
  (if (endp list)
    nil
    (or (and (first list) t)
        (any-true (rest list)))))

(defun all-true (list)
  (declare (xargs :guard (true-listp list)))
  (if (endp list)
    t
    (and (first list)
         (all-true (rest list)))))

(defun model-check-aux (term envs)
  (declare (xargs :guard (and (exprp term) (alist-listp envs))))
  (if (endp envs)
    '()
    (cons (eval-bool term (first envs))
          (model-check-aux term (rest envs)))))

(defun model-check (term)
  (declare (xargs :guard (exprp term)))
  (let ((results (model-check-aux term (generate-envs (vars term)))))
    (cond ((all-true results)
           :valid)
          ((any-true results)
           :satisfiable)
          (t
           :insatisfiable))))


;;; Desugaring Terms
;;; ================

(defun desugar (exp)
  (declare (xargs :guard (exprp exp)))
  (cond ((atom exp)
         (if (symbolp exp)
           exp
           nil))
        ((and (true-listp exp) (equal (length exp) 2))
         (if (equal (first exp) 'not)
           `(not ,(desugar (second exp)))
           nil))
        ((and (true-listp exp) (equal (length exp) 3))
         (if (member (first exp) *operators*)
           (let ((lhs (desugar (second exp)))
                 (rhs (desugar (third exp))))
             (case (first exp)
                   (or       `(or ,lhs ,rhs))
                   (and      `(not (or (not ,lhs) (not ,rhs))))
                   (implies  `(or (not ,lhs) ,rhs))
                   (iff      `(or (not (or (not ,lhs) (not ,rhs)))
                                  (not (or ,lhs ,rhs))))
                   (xor      `(or (not (or ,lhs (not ,rhs)))
                                  (not (or (not ,lhs) ,rhs))))
                   (otherwise nil)))
           nil))
        (t nil)))

(thm (and (equal (desugar '(implies (and a b) c))
                 '(or (not (not (or (not a) (not b)))) c)) 
          (equal (desugar '(and (implies a b) (xor (iff a b) c)))
                 '(not (or (not (or (not a) b))
                           (not (or (not (or (or (not (or (not a) (not b)))
                                                 (not (or a b)))
                                             (not c)))
                                    (not (or (not (or (not (or (not a) (not b)))
                                                      (not (or a b))))
                                             c)))))))))

(defthm desugaring-preserves-expressions
  (implies (exprp exp)
           (exprp (desugar exp))))
  
(defthm desugaring-preserves-meaning
  (implies (exprp exp)
           (equal (eval-bool exp env) (eval-bool (desugar exp) env))))#|ACL2s-ToDo-Line|#

