; ****************** BEGIN INITIALIZATION FOR ACL2s MODE ****************** ;
; (Nothing to see here!  Your actual file is after this initialization code);

#+acl2s-startup (er-progn (assign fmt-error-msg "Problem loading the CCG book.~%Please choose \"Recertify ACL2s system books\" under the ACL2s menu and retry after successful recertification.") (value :invisible))
(include-book "ccg/ccg" :uncertified-okp nil :dir :acl2s-modes :ttags ((:ccg)) :load-compiled-file nil);v4.0 change

;Common base theory for all modes.
#+acl2s-startup (er-progn (assign fmt-error-msg "Problem loading ACL2s base theory book.~%Please choose \"Recertify ACL2s system books\" under the ACL2s menu and retry after successful recertification.") (value :invisible))
(include-book "base-theory" :dir :acl2s-modes)


#+acl2s-startup (er-progn (assign fmt-error-msg "Problem loading ACL2s customizations book.~%Please choose \"Recertify ACL2s system books\" under the ACL2s menu and retry after successful recertification.") (value :invisible))
(include-book "custom" :dir :acl2s-modes :uncertified-okp nil :ttags :all)

#+acl2s-startup (er-progn (assign fmt-error-msg "Problem setting up ACL2s mode.") (value :invisible))

;Settings common to all ACL2s modes
(acl2s-common-settings)
;(acl2::xdoc acl2s::defunc) ;; 3 seconds is too much time to spare -- commenting out [2015-02-01 Sun]

; Non-events:
;(set-guard-checking :none)

(acl2::in-package "ACL2S")

; ******************* END INITIALIZATION FOR ACL2s MODE ******************* ;
;$ACL2s-SMode$;ACL2s
(in-package "ACL2")

(defun sm-pop (stk)
  (rest stk))

(defun sm-top (stk)
  (if (consp stk)
    (first stk)
    0))

(defun sm-push (val stk)
  (cons val stk))

;;; Perform one step of the stack machine when the current
;;; instruction is ins, the environment alist, and the stack
;;; stk.

(defun sm-step (ins stk)
  (let ((op (first ins)))
    (case op
          (pushc (sm-push (second ins) stk))
          (dup   (sm-push (sm-top stk) stk))
          (add   (sm-push (+ (sm-top (sm-pop stk)) (sm-top stk))
                          (sm-pop (sm-pop stk))))
          (mul   (sm-push (* (sm-top (sm-pop stk)) (sm-top stk))
                          (sm-pop (sm-pop stk))))
          (t stk))))

;;; sm-run runs a program in an environment alist starting with stack
;;; stk.

(defun sm-run (program stk)
  (if (endp program)
    stk
    (sm-run (rest program)
            (sm-step (first program) stk))))

(defun sm-states (program stk)
  (if (endp program)
    (list stk)
    (cons stk
          (sm-states (rest program)
                     (sm-step (first program) stk)))))

(sm-states '((pushc 1)
             (pushc 2)
             (dup)
             (add)
             (pushc 3)
             (mul))
           '())

(defun stack-depth (stk)
  (length stk))

(defun map-stack-depth (states)
  (if (endp states)
    '()
    (cons (stack-depth (first states))
          (map-stack-depth (rest states)))))

(defun sm-stack-depths (program stk)
  (if (endp program)
    (list (stack-depth stk))
    (cons (stack-depth stk)
          (sm-stack-depths
           (rest program)
           (sm-step (first program) stk)))))

(sm-stack-depths '((pushc 1)
                   (pushc 2)
                   (dup)
                   (add)
                   (pushc 3)
                   (mul))
                 '())

(sm-stack-depths '((pushc 1)
                   (pushc 2)
                   (dup)
                   (dup)
                   (pushc 3)
                   (mul))
                 '())

(defthm stack-depth-for-single-step
  (<= (stack-depth (sm-step ins stk))
      (1+ (stack-depth stk))))#|ACL2s-ToDo-Line|#

