; *************** BEGIN INITIALIZATION FOR PROGRAMMING MODE *************** ;
; (Nothing to see here!  Your actual file is after this initialization code);

#+acl2s-startup (er-progn (assign fmt-error-msg "Problem loading the TRACE* book.~%Please choose \"Recertify ACL2s system books\" under the ACL2s menu and retry after successful recertification.") (value :invisible))
; only load for interactive sessions: 
#+acl2s-startup (include-book "trace-star" :uncertified-okp nil :dir :acl2s-modes :ttags ((:acl2s-interaction)) :load-compiled-file nil);v4.0 change

#+acl2s-startup (er-progn (assign fmt-error-msg "Problem loading the EVALABLE-LD-PRINTING book.~%Please choose \"Recertify ACL2s system books\" under the ACL2s menu and retry after successful recertification.") (value :invisible))
; only load for interactive sessions: 
#+acl2s-startup (include-book "hacking/evalable-ld-printing" :uncertified-okp nil :dir :system :ttags ((:evalable-ld-printing)) :load-compiled-file nil);v4.0 change


#+acl2s-startup (er-progn (assign fmt-error-msg "Problem loading ACL2s customizations book.~%Please choose \"Recertify ACL2s system books\" under the ACL2s menu and retry after successful recertification.") (value :invisible))
(include-book "defunc" :dir :acl2s-modes :uncertified-okp nil :load-compiled-file :comp) ;lets add defunc at least harshrc [2015-02-01 Sun]
(include-book "custom" :dir :acl2s-modes :uncertified-okp nil :load-compiled-file :comp)

#+acl2s-startup (er-progn (assign fmt-error-msg "Problem loading programming mode.") (value :invisible))


(er-progn 
  (program)
  (defun book-beginning () ()) ; to prevent book development
  (set-irrelevant-formals-ok :warn)
  (set-bogus-mutual-recursion-ok :warn)
  (set-ignore-ok :warn)
  (set-verify-guards-eagerness 0)
  (set-default-hints '(("Goal" :error "This depends on a proof, and proofs are disabled in Programming mode.  The session mode can be changed under the \"ACL2s\" menu.")))
  (reset-prehistory t)
  (set-guard-checking :none)
  (assign evalable-ld-printingp t)
  (assign evalable-printing-abstractions '(list cons))
  (assign triple-print-prefix "; "))
  

(cw "~@0Programming mode loaded.~%~@1"
      #+acl2s-startup "${NoMoReSnIp}$~%" #-acl2s-startup ""
      #+acl2s-startup "${SnIpMeHeRe}$~%" #-acl2s-startup "")

; **************** END INITIALIZATION FOR PROGRAMMING MODE **************** ;
;$ACL2s-SMode$;Programming
;;;; Funktionsaufruf
;;;; ===============

(+ 1 2)
(+ (* 2 3) (* 4 5))
(expt 2 8)

(if t 1 2)
(if nil 1 2)

;;;; Bäume und Listen
;;;; ================

(car '(1 . 2))
(cdr '(1 . 2))

(car '((1 . 2) . (3 . 4)))
(cdr '((1 . 2) . (3 . 4)))

(car '())
(cdr '())

(car '(1 2 3))
(cdr '(1 2 3))

(first '(1 2 3))
(rest '(1 2 3))

;;;; Intermezzo: Macros
;;;; ==================

;;; +

:trans (+ 1 2)
:trans (+ 1 2 3)
:trans (+ 1 2 3 4)
:trans (+ 1 2 3 4 5)
:trans (+ 1)
:trans (+)

;;; Gleichheit

;;; equal

(equal 1 1)
(equal 1 2)

;;;; Kontrollstrukturen
;;;; ==================

;;; if

(if (< 2 3) 'foo 'bar)

;;; case

(case 1
      (0 'zero)
      (1 'one)
      (2 'two))

(case 6
      ((0 1 2) 'small)
      ((3 4 5) 'medium)
      (otherwise 'large))

;;; cond

(cond ((equal 1 2) 'huh?)
      ((equal 2 3) 'yeah)
      (t 'default))

;;;; Variablenbindung
;;;; ================

;;; let

(let ((x 1)
      (y 2))
  (+ x y))

(let ((x 1))
  (let ((x 2)
        (y x))
    y))

(let ((x 1))
  (let* ((x 2)
         (y x))
    y))

(defun my-fun (x y)
  (+ x x y))

(defun your-fun ()
  'constant)

(my-fun 1 2)
(your-fun)

;;;; Vordefinierte Funktionen und Macros
;;;; ===================================

;;; Logik
;;; -----

;;; and
(and t t)
(and nil t)
(and t nil)
(and t 123)
(and nil 123)
(and 1 2)
(and 1 2 3 4 5 6)

;;; or
(or nil nil)
(or t nil)
(or nil 123 234)

;;; implies
(implies t t)
(implies t nil)
(implies nil t)
(implies nil nil)

;;; not

(not t)
(not nil)
(not 'foo)

;;; iff

(iff t t)
(iff nil nil)
(iff t nil)
(iff nil t)
(iff 1 2)

;;; Arithmetik
;;; ----------

;;; acl2-numberp
;;; integerp
(integerp 1)
(integerp -1)
(integerp 2/3)
(integerp 'foo)

;;; rationalp
;;; complex-rationalp
;;; zerop

(zerop 0)
(zerop 1)
(zerop -1)

;;; zip
;;; zp
(zp 0)
(zp 1)
(zp -1)
(zp 'foo)

;;; <
;;; <=
;;; >
;;; >=
;;; +
;;; *
;;; - (unary and binary)
;;; /

;;; 1-
(1- 3)

;;; 1+
(1+ 2)

;;; numerator
;;; denominator
;;; realpart
;;; imgpart
;;; complex

;;; Zeichen
;;; -------

;;; characterp
(characterp #\a)
(characterp "a")

;;; char-code
;;; code-char

;;; Strings
;;; -------

;;; stringp
;;; (char <string> <n>)

(char "asdf" 1)

;;; (coerce <string> 'list)
(coerce "foo" 'list)
;;; (coerce <charlist> 'string)
;;; length

(length "foo")

;;; Symbole
;;; -------

;;; symbolp
(symbolp 'foo)
(symbolp "foo")

;;; symbol-name
(symbol-name 'foo)

;;; symbol-package-name
(symbol-package-name 'foo)

;;; intern-in-package-of-symbol

;;; Cons-Zellen, Paare und Listen
;;; -----------------------------

;;; consp
(consp '(1 . 2))
(consp '(1 2 3))
(consp '())

;;; cons
(cons 1 2)
(equal '(1 . 2) (cons 1 2))
(cons (1+ 2) (1- 2))

;;; car
;;; cdr

;;; endp
;;; atom

(endp '())
(endp '(1 2 3))
(endp 1)
(atom 1)

;;; list
(cons 1 (cons 2 (cons 3 nil)))
(list 1 2 3)
'((+ 1 2) (1- 3))
(list (+ 1 2) (1- 3))


;;; list*
(list* 1 2 3 4 '(a b c))

;;; caar
(car (car '((1 2 3))))
(caar '((1 2 3)))

;;; cadr
;;; cdar
;;; cddr
;;; ...

;;; append
(append '(1 2 3) '(3 4 5))
(list* '(1 2 3) '(3 4 5))

;;; member-equal
(member-equal 2 '(1 2 3))

;;; assoc-equal
(assoc-equal 'x '((x . 1) (y . 2)))

;;; (nth <n> <list>)
(nth 1 '(1 2 3))

;;; length
(length '(1 2 3))

;;; len
;;; true-listp
(true-listp '(1 2 3))
(true-listp '(1 . 2))
(true-listp 123)#|ACL2s-ToDo-Line|#


;;; Definitionen 
;;; ------------

;;; (defpkg <name> '(s_1 ... s_n))
;;; (defconst <*sym*> 'const)
;;; (defmacro <name> <args> <body>)
;;; (defun <f> <args> <body>

;;; (mutual-recursion
;;;   (defun <f1> ...)
;;;   (defun <f2> ...))

;;; (ld "file")

;;; Top-Level Kommandos

;;; :program
;;; :logic
;;; :redef
;;; :set-guard-checking t/nil
;;; (in-package <pkg>)

;;; :u (undo)
;;; :pbt k (print back)
;;; :ubt k (undo back)
;;; :pc <name> (print command defining)
;;; :doc