; ****************** BEGIN INITIALIZATION FOR ACL2s MODE ****************** ;
; (Nothing to see here!  Your actual file is after this initialization code);

#+acl2s-startup (er-progn (assign fmt-error-msg "Problem loading the CCG book.~%Please choose \"Recertify ACL2s system books\" under the ACL2s menu and retry after successful recertification.") (value :invisible))
(include-book "ccg/ccg" :uncertified-okp nil :dir :acl2s-modes :ttags ((:ccg)) :load-compiled-file nil);v4.0 change

;Common base theory for all modes.
#+acl2s-startup (er-progn (assign fmt-error-msg "Problem loading ACL2s base theory book.~%Please choose \"Recertify ACL2s system books\" under the ACL2s menu and retry after successful recertification.") (value :invisible))
(include-book "base-theory" :dir :acl2s-modes)


#+acl2s-startup (er-progn (assign fmt-error-msg "Problem loading ACL2s customizations book.~%Please choose \"Recertify ACL2s system books\" under the ACL2s menu and retry after successful recertification.") (value :invisible))
(include-book "custom" :dir :acl2s-modes :uncertified-okp nil :ttags :all)

#+acl2s-startup (er-progn (assign fmt-error-msg "Problem setting up ACL2s mode.") (value :invisible))

;Settings common to all ACL2s modes
(acl2s-common-settings)
;(acl2::xdoc acl2s::defunc) ;; 3 seconds is too much time to spare -- commenting out [2015-02-01 Sun]

; Non-events:
;(set-guard-checking :none)

(acl2::in-package "ACL2S")

; ******************* END INITIALIZATION FOR ACL2s MODE ******************* ;
;$ACL2s-SMode$;ACL2s
(set-termination-method :measure)
(set-gag-mode nil)

(defun flatten-tree (x)
  (if (atom x) 
      (list x)
      (append (flatten-tree (car x))
              (flatten-tree (cdr x)))))

(defun flatten-tree-1 (x)
  (cond ((atom x) (list x))
        (t (append (flatten-tree-1 (car x))
                   (flatten-tree-1 (cdr x))))))

(defun swap-tree (x)
  (if (atom x)
    x
    (cons (swap-tree (cdr x))
          (swap-tree (car x)))))

(defthm flatten-swap-rev
  (equal (flatten-tree (swap-tree x))
         (rev (flatten-tree x))))#|ACL2s-ToDo-Line|#


(defun sum-squares (n)
  (if (zp n) 0
    (+ (* n n) (sum-squares (- n 1)))))

(defthm sum-squares-closed-form
  (implies (natp n)
           (equal (sum-squares n)
                  (/ (* n (+ n 1) (+ (* 2 n) 1)) 6))))