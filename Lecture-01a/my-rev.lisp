; ****************** BEGIN INITIALIZATION FOR ACL2s MODE ****************** ;
; (Nothing to see here!  Your actual file is after this initialization code);

#+acl2s-startup (er-progn (assign fmt-error-msg "Problem loading the CCG book.~%Please choose \"Recertify ACL2s system books\" under the ACL2s menu and retry after successful recertification.") (value :invisible))
(include-book "ccg/ccg" :uncertified-okp nil :dir :acl2s-modes :ttags ((:ccg)) :load-compiled-file nil);v4.0 change

;Common base theory for all modes.
#+acl2s-startup (er-progn (assign fmt-error-msg "Problem loading ACL2s base theory book.~%Please choose \"Recertify ACL2s system books\" under the ACL2s menu and retry after successful recertification.") (value :invisible))
(include-book "base-theory" :dir :acl2s-modes)


#+acl2s-startup (er-progn (assign fmt-error-msg "Problem loading ACL2s customizations book.~%Please choose \"Recertify ACL2s system books\" under the ACL2s menu and retry after successful recertification.") (value :invisible))
(include-book "custom" :dir :acl2s-modes :uncertified-okp nil :ttags :all)

#+acl2s-startup (er-progn (assign fmt-error-msg "Problem setting up ACL2s mode.") (value :invisible))

;Settings common to all ACL2s modes
(acl2s-common-settings)
;(acl2::xdoc acl2s::defunc) ;; 3 seconds is too much time to spare -- commenting out [2015-02-01 Sun]

; Non-events:
;(set-guard-checking :none)

(acl2::in-package "ACL2S")

; ******************* END INITIALIZATION FOR ACL2s MODE ******************* ;
;$ACL2s-SMode$;ACL2s
;;; Here is the definition of my-rev again, but this time evaluated
;;; in a logic mode (ACL2s).  Note that admitting the function
;;; causes ACL2 to prove termination. 

(defun my-rev (xs)
  "my-reverse the list xs"
  (if (atom xs)
    nil
    (append (my-rev (rest xs)) (list (first xs)))))

;;; Now we can prove our claim about my-rev:

(defthm my-rev^2-identity-for-true-listp
  (implies (true-listp xs)
           (equal (my-rev (my-rev xs)) xs)))

;;; We also see that the proof will fail without the condition:
#+(or)
(defthm my-rev^2-identity  
  (equal (my-rev (my-rev xs)) xs))

;;; What else holds for my-rev?

(defthm my-rev-keeps-members
  (iff (member x (my-rev xs))
       (member x xs)))

(defthm my-rev-len
  (equal (len (my-rev xs))
         (len xs)))

(defthm my-rev-nth
  (implies (and (natp n) (< n (len xs)) (true-listp xs))
           (equal (nth n (my-rev xs))
                  (nth (1- (- (len xs) n)) xs)))
  :rule-classes nil)