; ****************** BEGIN INITIALIZATION FOR ACL2s MODE ****************** ;
; (Nothing to see here!  Your actual file is after this initialization code);

#+acl2s-startup (er-progn (assign fmt-error-msg "Problem loading the CCG book.~%Please choose \"Recertify ACL2s system books\" under the ACL2s menu and retry after successful recertification.") (value :invisible))
(include-book "ccg/ccg" :uncertified-okp nil :dir :acl2s-modes :ttags ((:ccg)) :load-compiled-file nil);v4.0 change

;Common base theory for all modes.
#+acl2s-startup (er-progn (assign fmt-error-msg "Problem loading ACL2s base theory book.~%Please choose \"Recertify ACL2s system books\" under the ACL2s menu and retry after successful recertification.") (value :invisible))
(include-book "base-theory" :dir :acl2s-modes)


#+acl2s-startup (er-progn (assign fmt-error-msg "Problem loading ACL2s customizations book.~%Please choose \"Recertify ACL2s system books\" under the ACL2s menu and retry after successful recertification.") (value :invisible))
(include-book "custom" :dir :acl2s-modes :uncertified-okp nil :ttags :all)

#+acl2s-startup (er-progn (assign fmt-error-msg "Problem setting up ACL2s mode.") (value :invisible))

;Settings common to all ACL2s modes
(acl2s-common-settings)
;(acl2::xdoc acl2s::defunc) ;; 3 seconds is too much time to spare -- commenting out [2015-02-01 Sun]

; Non-events:
;(set-guard-checking :none)

(acl2::in-package "ACL2S")

; ******************* END INITIALIZATION FOR ACL2s MODE ******************* ;
;$ACL2s-SMode$;ACL2s
(defun is-sorted-p (xs)
  (if (or (endp xs) (endp (rest xs)))
    t
    (and (<= (first xs) (second xs))
         (is-sorted-p (rest xs)))))

(defthm sorted-examples
  (and (is-sorted-p '())
       (is-sorted-p '(1))
       (is-sorted-p '(1 2 3))
       (not (is-sorted-p '(3 2 1)))))

(defun insert (x xs)
  (cond ((atom xs)
         (list x))
        ((< x (first xs))
         (cons x xs))
        (t
         (cons (first xs) (insert x (rest xs))))))

(defthm insert-keeps-list-sorted
  (implies (is-sorted-p xs)
           (is-sorted-p (insert x xs))))

(defthm insert-inserts-a-single-element
  (implies (listp xs)
           (equal (length (insert x xs))
                  (1+ (length xs)))))

(defthm insert-keeps-old-elements
  (implies (member x xs)
           (member x (insert y xs))))

(defthm characterize-elements-after-insertion
  (iff (member x (insert y xs))
       (or (member x xs) (equal x y))))

(defun insertion-sort-helper (xs sorted)
  (if (endp xs)
    sorted
    (insertion-sort-helper (rest xs)
                           (insert (first xs) sorted))))

(defthm insertion-sort-helper-keeps-elts-sorted
  (implies (is-sorted-p sorted)
           (is-sorted-p (insertion-sort-helper xs sorted))))

(defthm insertion-sort-helper-keeps-elements-of-sorted-list
  (implies (member x sorted)
           (member x (insertion-sort-helper xs sorted))))

(defthm insertion-sort-helper/member/1
  (implies (not (member x xs))
           (implies (member x (insertion-sort-helper xs sorted))
                    (member x sorted))))

(defthm insertion-sort-helper-keeps-elements-of-input-list
  (implies (member x xs)
           (member x (insertion-sort-helper xs sorted))))

(defthm characterize-elements-of-inserion-sort-helper-call
  (iff (member x (insertion-sort-helper xs sorted))
       (or (member x xs) (member x sorted))))

(defun insertion-sort (xs)
  (insertion-sort-helper xs '()))

(defthm result-of-insertion-sort-is-sorted
  (is-sorted-p (insertion-sort xs))
  :rule-classes ())

(defthm characterize-result-of-insertion-sort
  (iff (member x (insertion-sort xs))
       (member x xs))
  :rule-classes ())
