; ****************** BEGIN INITIALIZATION FOR ACL2s MODE ****************** ;
; (Nothing to see here!  Your actual file is after this initialization code);

#+acl2s-startup (er-progn (assign fmt-error-msg "Problem loading the CCG book.~%Please choose \"Recertify ACL2s system books\" under the ACL2s menu and retry after successful recertification.") (value :invisible))
(include-book "ccg/ccg" :uncertified-okp nil :dir :acl2s-modes :ttags ((:ccg)) :load-compiled-file nil);v4.0 change

;Common base theory for all modes.
#+acl2s-startup (er-progn (assign fmt-error-msg "Problem loading ACL2s base theory book.~%Please choose \"Recertify ACL2s system books\" under the ACL2s menu and retry after successful recertification.") (value :invisible))
(include-book "base-theory" :dir :acl2s-modes)


#+acl2s-startup (er-progn (assign fmt-error-msg "Problem loading ACL2s customizations book.~%Please choose \"Recertify ACL2s system books\" under the ACL2s menu and retry after successful recertification.") (value :invisible))
(include-book "custom" :dir :acl2s-modes :uncertified-okp nil :ttags :all)

#+acl2s-startup (er-progn (assign fmt-error-msg "Problem setting up ACL2s mode.") (value :invisible))

;Settings common to all ACL2s modes
(acl2s-common-settings)
;(acl2::xdoc acl2s::defunc) ;; 3 seconds is too much time to spare -- commenting out [2015-02-01 Sun]

; Non-events:
;(set-guard-checking :none)

(acl2::in-package "ACL2S")

; ******************* END INITIALIZATION FOR ACL2s MODE ******************* ;
;$ACL2s-SMode$;ACL2s
(defun is-sorted-p (xs)
  (if (or (endp xs) (endp (rest xs)))
    t
    (and (<= (first xs) (second xs))
         (is-sorted-p (rest xs)))))

(defthm sorted-examples
  (and (is-sorted-p '())
       (is-sorted-p '(1))
       (is-sorted-p '(1 2 3))
       (not (is-sorted-p '(3 2 1)))))

(defun insert (x xs)
  (cond ((atom xs)
         (list x))
        ((<= x (first xs))
         (cons x xs))
        (t
         (cons (first xs) (insert x (rest xs))))))

(defthm insert-keeps-list-sorted
  (implies (is-sorted-p xs)
           (is-sorted-p (insert x xs)))
  :rule-classes nil)

(defthm insert-inserts-a-single-element
  (implies (listp xs)
           (equal (length (insert x xs))
                  (1+ (length xs))))
  :rule-classes nil)

(defun insertion-sort (xs)
  (if (atom xs)
    '()
    (insert (first xs)
            (insertion-sort (rest xs)))))

(defun in (x xs)
  (cond ((atom xs) nil)
        ((equal x (first xs)) t)
        (t (in x (rest xs)))))

(defthm nothing-is-in-nil
  (not (in x '()))
  :rule-classes nil)

(defthm in-first-or-in-rest
  (implies (in x xs)
           (or (equal x (first xs))
               (in x (rest xs))))
  :rule-classes nil)

(defun del (x xs)
  (cond ((endp xs) '())
        ((equal x (first xs)) (rest xs))
        (t (cons (first xs)
                 (del x (rest xs))))))

(defthm del-does-not-add-members
  (implies (not (in x xs))
           (not (in x (del y xs))))
  :rule-classes nil)

(defthm del-deletes-one-element
  (implies (in x xs)
           (= (len (del x xs)) (1- (len xs))))
  :rule-classes nil)

(defun perm (xs ys)
  (if (atom xs)
    (atom ys)
    (and (in (first xs) ys)
         (perm (rest xs) (del (first xs) ys)))))

(defthm insert-perm-cons
  (implies (perm xs ys)
           (perm (insert x xs) (cons x ys))))

(defthm insertion-sort-is-perm
  (perm (insertion-sort xs) xs))

