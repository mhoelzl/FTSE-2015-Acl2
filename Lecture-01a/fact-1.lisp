; ****************** BEGIN INITIALIZATION FOR ACL2s MODE ****************** ;
; (Nothing to see here!  Your actual file is after this initialization code);

#+acl2s-startup (er-progn (assign fmt-error-msg "Problem loading the CCG book.~%Please choose \"Recertify ACL2s system books\" under the ACL2s menu and retry after successful recertification.") (value :invisible))
(include-book "ccg/ccg" :uncertified-okp nil :dir :acl2s-modes :ttags ((:ccg)) :load-compiled-file nil);v4.0 change

;Common base theory for all modes.
#+acl2s-startup (er-progn (assign fmt-error-msg "Problem loading ACL2s base theory book.~%Please choose \"Recertify ACL2s system books\" under the ACL2s menu and retry after successful recertification.") (value :invisible))
(include-book "base-theory" :dir :acl2s-modes)


#+acl2s-startup (er-progn (assign fmt-error-msg "Problem loading ACL2s customizations book.~%Please choose \"Recertify ACL2s system books\" under the ACL2s menu and retry after successful recertification.") (value :invisible))
(include-book "custom" :dir :acl2s-modes :uncertified-okp nil :ttags :all)

#+acl2s-startup (er-progn (assign fmt-error-msg "Problem setting up ACL2s mode.") (value :invisible))

;Settings common to all ACL2s modes
(acl2s-common-settings)
;(acl2::xdoc acl2s::defunc) ;; 3 seconds is too much time to spare -- commenting out [2015-02-01 Sun]

; Non-events:
;(set-guard-checking :none)

(acl2::in-package "ACL2S")

; ******************* END INITIALIZATION FOR ACL2s MODE ******************* ;
;$ACL2s-SMode$;ACL2s
(defun fact (n)
  (if (zp n)
    1
    (* n (fact (1- n)))))

(defun mult (k n)
  (cond ((or (not (natp n)) (not (natp k)) (< n k))
         1)
        ((= n k)
         n)
        (t
         (* n (mult k (1- n))))))

(defthm mult-upper-bound
  (implies (and (posp k) (natp n) (<= k n))
           (equal (mult k n)
                  (* n (mult k (1- n)))))
  :rule-classes nil)

(defthm mult-lower-bound
  (implies (and (natp k) (natp n) (<= k n))
           (equal (mult k n)
                  (* k (mult (1+ k) n))))
  :rule-classes nil)

(defthm mult-1-to-n-is-fact
  (equal (fact n) (mult 1 n)))

(defun fact-iter (n acc)
  (if (zp n)
    (fix acc)
    (fact-iter (1- n) (* n acc))))

(defthm fact-equals-fact-iter
  (equal (fact-iter n 1)
         (fact n)))

(defthm fact-iter-+1
  (implies (and (natp n) (< 0 n) (natp acc))
           (equal (fact-iter (1- n) (* n acc))
                  (fact-iter n acc))))

#+(or)
(defthm fact-iter-mult-fail
  (implies (and (posp k) (natp n) (< k n))
           (equal (fact-iter (1- k) (mult k n))
                  (fact-iter k (mult (1+ k) n)))))

(defthm fact-iter-mult
  (implies (and (posp k) (natp n) (< k n))
           (equal (fact-iter (1- k) (mult k n))
                  (fact-iter k (mult (1+ k) n))))
  :hints (("Goal"
           :use ((:instance mult-lower-bound)))))
