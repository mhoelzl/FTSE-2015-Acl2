; ****************** BEGIN INITIALIZATION FOR ACL2s MODE ****************** ;
; (Nothing to see here!  Your actual file is after this initialization code);

#+acl2s-startup (er-progn (assign fmt-error-msg "Problem loading the CCG book.~%Please choose \"Recertify ACL2s system books\" under the ACL2s menu and retry after successful recertification.") (value :invisible))
(include-book "ccg/ccg" :uncertified-okp nil :dir :acl2s-modes :ttags ((:ccg)) :load-compiled-file nil);v4.0 change

;Common base theory for all modes.
#+acl2s-startup (er-progn (assign fmt-error-msg "Problem loading ACL2s base theory book.~%Please choose \"Recertify ACL2s system books\" under the ACL2s menu and retry after successful recertification.") (value :invisible))
(include-book "base-theory" :dir :acl2s-modes)


#+acl2s-startup (er-progn (assign fmt-error-msg "Problem loading ACL2s customizations book.~%Please choose \"Recertify ACL2s system books\" under the ACL2s menu and retry after successful recertification.") (value :invisible))
(include-book "custom" :dir :acl2s-modes :uncertified-okp nil :ttags :all)

#+acl2s-startup (er-progn (assign fmt-error-msg "Problem setting up ACL2s mode.") (value :invisible))

;Settings common to all ACL2s modes
(acl2s-common-settings)
;(acl2::xdoc acl2s::defunc) ;; 3 seconds is too much time to spare -- commenting out [2015-02-01 Sun]

; Non-events:
;(set-guard-checking :none)

(acl2::in-package "ACL2S")

; ******************* END INITIALIZATION FOR ACL2s MODE ******************* ;
;$ACL2s-SMode$;ACL2s
;;; Define the valid expressions of the input language.

(defun exprp (exp)
  "Check whether exp is a valid expression"
  (cond ((atom exp)
         ;; Only symbols and numbers are allowed, no strings or
         ;; characters
         (or (symbolp exp) (acl2-numberp exp)))
        ((equal (len exp) 2)
         (and (or (equal (first exp) 'inc)
                  (equal (first exp) 'sq))
              (exprp (second exp))))
        (t
         (and (equal (len exp) 3)
              (or (equal (second exp) '+)
                  (equal (second exp) '*))
              (exprp (first exp))
              (exprp (third exp))))))

(thm (and (exprp 'foo)
          (exprp 'x!)
          (exprp 1)
          (exprp 1/2)
          (exprp -1)
          (not (exprp "String"))
          (not (exprp #\x))
          (exprp '(inc x))
          (exprp '(sq 17/4))
          (not (exprp '(dec x)))
          (not (exprp '(inc "String")))
          (not (exprp '(inc x y)))
          (exprp '(1 + x))
          (exprp '(2 * (7 + (inc y))))
          (exprp '(((sq (1 + 7)) + 2) * ((inc 3) + (sq 4))))))


;;; Environment handling.

(defun lookup (var alist)
  (cond ((endp alist)
         ;; All variables default to 0
         0)
        ((equal var (car (first alist)))
         (cdr (first alist)))
        (t (lookup var (rest alist)))))

(thm (and (equal (lookup 'x '((x . 1) (y . 2))) 1)
          (equal (lookup 'y '((x . 1) (y . 2))) 2)
          (equal (lookup 'z '((x . 1) (y . 2))) 0)))


;;; An interpreter.

(defun eval-expr (exp alist)
  (cond (;; Evaluate atoms: either look up variable value or return
         ;; the number.
         (atom exp)
         (if (symbolp exp)
           (lookup exp alist)
           exp))
        (;; Unary operators
         (equal (len exp) 2)
         (case (first exp)
               ((inc) (1+ (eval-expr (second exp) alist)))
               ;; Only other operator is sq
               (otherwise (* (eval-expr (second exp) alist)
                             (eval-expr (second exp) alist)))))
        (;; Binary operators
         t
         (case (second exp)
               ((+) (+ (eval-expr (first exp) alist)
                     (eval-expr (third exp) alist)))
               (otherwise (* (eval-expr (first exp) alist)
                             (eval-expr (third exp) alist)))))))

(thm 
 (let ((alist '((x . 1) (y . 2) (z . 3))))
   (and (equal (eval-expr 10 alist) 10)
        (equal (eval-expr 'x alist) 1)
        (equal (eval-expr '(x + y) alist) 3)
        (equal (eval-expr '(2 * ((inc (-3 + (sq z))) * (inc y))) alist) 42))))

;;; A stack machine.

(defun sm-pop (stk)
  (rest stk))

(defun sm-top (stk)
  (if (consp stk)
    (first stk)
    0))

(defun sm-push (val stk)
  (cons val stk))

;;; Perform one step of the stack machine when the current
;;; instruction is ins, the environment alist, and the stack
;;; stk.

(defun sm-step (ins alist stk)
  (let ((op (first ins)))
    (case op
          (pushv (sm-push (lookup (second ins) alist) stk))
          (pushc (sm-push (second ins) stk))
          (dup   (sm-push (sm-top stk) stk))
          (add   (sm-push (+ (sm-top (sm-pop stk)) (sm-top stk))
                          (sm-pop (sm-pop stk))))
          (mul   (sm-push (* (sm-top (sm-pop stk)) (sm-top stk))
                          (sm-pop (sm-pop stk))))
          (t stk))))

;;; sm-run a program in an environment alist starting with stack
;;; stk.

(defun sm-run (program alist stk)
  (if (endp program)
    stk
    (sm-run (rest program)
         alist
         (sm-step (first program) alist stk))))

(defun sm-compile  (exp)
  (cond
   (;; sm-compile  atoms
    (atom exp)
    (if (symbolp exp)
      ;; Look up symbols in the environment and push the
      ;; result on the stack
      (list (list 'pushv exp))
      ;; Push numbers on the stack
      (list (list 'pushc exp))))
   ((equal (len exp) 2)
    (if (equal (first exp) 'inc)
      ;; sm-compile  the argument followed by instructions to
      ;; add 1 to the result
      (append (sm-compile  (second exp))
              '((pushc 1) (add)))
      ;; Not inc, must be sq: sm-compile  the argument, duplicate
      ;; the value on the stack and multiply
      (append (sm-compile  (second exp))
              '((dup) (mul)))))
   (t
    (if (equal (second exp) '+)
      (append (sm-compile  (first exp))
              (sm-compile  (third exp))
              '((add)))
      (append (sm-compile  (first exp))
              (sm-compile  (third exp))
              '((mul)))))))

#+(or)
(defthm compile-is-correct
  (implies (exprp exp)
           (equal (sm-top (sm-run (sm-compile exp) alist stk))
                  (eval-expr exp alist))))

(defthm composition
  (equal (sm-run (append prg1 prg2) alist stk)
         (sm-run prg2 alist (sm-run prg1 alist stk))))

#+(or)
(defthm compile-is-correct
  (implies (exprp exp)
           (equal (sm-top (sm-run (sm-compile exp) alist stk))
                  (eval-expr exp alist))))#|ACL2s-ToDo-Line|#


#+(or)
(defthm compile-is-correct-general
  (implies (exprp exp)
           (equal (sm-run (sm-compile exp) alist stk)
                  (cons (eval-expr exp alist) stk))))

(defun compiler-induct (exp alist stk)
  (cond
   ((atom exp) stk)
   ((equal (len exp) 2)
    (compiler-induct (second exp) alist stk))
   (t
    (append (compiler-induct (first exp) alist stk)
            (compiler-induct (third exp)
                             alist
                             (cons (eval-expr (first exp) alist)
                                   stk))))))

(defthm compile-is-correct-general
  (implies (exprp exp)
           (equal (sm-run (sm-compile exp) alist stk)
                  (cons (eval-expr exp alist) stk)))
  :hints (("Goal"
           :induct (compiler-induct exp alist stk))))
