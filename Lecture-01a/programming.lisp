; *************** BEGIN INITIALIZATION FOR PROGRAMMING MODE *************** ;
; (Nothing to see here!  Your actual file is after this initialization code);

#+acl2s-startup (er-progn (assign fmt-error-msg "Problem loading the TRACE* book.~%Please choose \"Recertify ACL2s system books\" under the ACL2s menu and retry after successful recertification.") (value :invisible))
; only load for interactive sessions: 
#+acl2s-startup (include-book "trace-star" :uncertified-okp nil :dir :acl2s-modes :ttags ((:acl2s-interaction)) :load-compiled-file nil);v4.0 change

#+acl2s-startup (er-progn (assign fmt-error-msg "Problem loading the EVALABLE-LD-PRINTING book.~%Please choose \"Recertify ACL2s system books\" under the ACL2s menu and retry after successful recertification.") (value :invisible))
; only load for interactive sessions: 
#+acl2s-startup (include-book "hacking/evalable-ld-printing" :uncertified-okp nil :dir :system :ttags ((:evalable-ld-printing)) :load-compiled-file nil);v4.0 change


#+acl2s-startup (er-progn (assign fmt-error-msg "Problem loading ACL2s customizations book.~%Please choose \"Recertify ACL2s system books\" under the ACL2s menu and retry after successful recertification.") (value :invisible))
(include-book "defunc" :dir :acl2s-modes :uncertified-okp nil :load-compiled-file :comp) ;lets add defunc at least harshrc [2015-02-01 Sun]
(include-book "custom" :dir :acl2s-modes :uncertified-okp nil :load-compiled-file :comp)

#+acl2s-startup (er-progn (assign fmt-error-msg "Problem loading programming mode.") (value :invisible))


(er-progn 
  (program)
  (defun book-beginning () ()) ; to prevent book development
  (set-irrelevant-formals-ok :warn)
  (set-bogus-mutual-recursion-ok :warn)
  (set-ignore-ok :warn)
  (set-verify-guards-eagerness 0)
  (set-default-hints '(("Goal" :error "This depends on a proof, and proofs are disabled in Programming mode.  The session mode can be changed under the \"ACL2s\" menu.")))
  (reset-prehistory t)
  (set-guard-checking :none)
  (assign evalable-ld-printingp t)
  (assign evalable-printing-abstractions '(list cons))
  (assign triple-print-prefix "; "))
  

(cw "~@0Programming mode loaded.~%~@1"
      #+acl2s-startup "${NoMoReSnIp}$~%" #-acl2s-startup ""
      #+acl2s-startup "${SnIpMeHeRe}$~%" #-acl2s-startup "")

; **************** END INITIALIZATION FOR PROGRAMMING MODE **************** ;
;$ACL2s-SMode$;Programming
;;; This file is processed in programming mode, therefore functions
;;; that are not admissible according to the ACL2 logic can be entered
;;; and no termination checking is performed.


(defun fact (n)
  "Compute the factorial of n"
  (if (zp n)
    1
    (* n (fact (- n 1)))))

(fact 10)
(fact 100)

(defun fib (n)
  "Compute the Fibonacci function of n"
  (if (and (integerp n) (<= 2 n))
    (+ (fib (- n 1)) (fib (- n 2)))
    n))

(fib 10)

(defun endless (n)
  "Loop endlessly"
  (1+ (endless n)))

#+(or)
(endless 1)

(defun my-rev (xs)
  "my-reverse the list xs"
  (if (endp xs)
    nil
    (append (my-rev (rest xs)) (list (first xs)))))

;;; What is the result of evaluating (my-rev '(a b c d))?
(my-rev '(a b c d))

;;; Why does my-rev change the case of a, b, c, and d?

;;; What is (my-rev '("a" "b" "c" "d"))?
(my-rev '("a" "b" "c" "d"))

;;; What is (my-rev (a b c d))
#+(or)
(my-rev (a b c d))

;;; What is (my-rev "This is a string")?
(my-rev "This is a string")

;;; Why?
;;; What is (endp "This is a string")?
(endp "This is a string")

;;; What is (my-rev 123)?
(my-rev 123)

;;; What is (my-rev '(a . b))?
(my-rev '(a . b))

;;; What is (my-rev (cons 'a 'b))?
(my-rev (cons 'a 'b))

;;; What is (my-rev '(a b c . d))?
(my-rev '(a b c . d))#|ACL2s-ToDo-Line|#


;;; Are (my-rev (my-rev x)) and x always the same object?

;;; What condition must x satisfy that (my-rev (my-rev x)) are equal?
;;; How could we check this?