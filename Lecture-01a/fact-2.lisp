(defun fact (n)
  (if (zp n)
    1
    (* n (fact (1- n)))))

(defun fact-iter (n acc)
  (if (zp n)
    (fix acc)
    (fact-iter (1- n) (* n acc))))

#+(or)
(defthm fact=fact-iter/fail-1
  (equal (fact-iter n 1) (fact n)))

(include-book "arithmetic/top-with-meta" :dir :system)

#+(or)
(defthm fact=fact-iter/fail-2
  (equal (fact-iter n 1) (fact n)))

(defthm fact-iter=fact/lemma-1
  (equal (fact-iter n m)
         (* m (fact n))))

(defthm fact=fact-iter
  (equal (fact-iter n 1) (fact n)))

