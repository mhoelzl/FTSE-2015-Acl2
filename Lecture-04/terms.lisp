; ****************** BEGIN INITIALIZATION FOR ACL2s MODE ****************** ;
; (Nothing to see here!  Your actual file is after this initialization code);

#+acl2s-startup (er-progn (assign fmt-error-msg "Problem loading the CCG book.~%Please choose \"Recertify ACL2s system books\" under the ACL2s menu and retry after successful recertification.") (value :invisible))
(include-book "ccg/ccg" :uncertified-okp nil :dir :acl2s-modes :ttags ((:ccg)) :load-compiled-file nil);v4.0 change

;Common base theory for all modes.
#+acl2s-startup (er-progn (assign fmt-error-msg "Problem loading ACL2s base theory book.~%Please choose \"Recertify ACL2s system books\" under the ACL2s menu and retry after successful recertification.") (value :invisible))
(include-book "base-theory" :dir :acl2s-modes)


#+acl2s-startup (er-progn (assign fmt-error-msg "Problem loading ACL2s customizations book.~%Please choose \"Recertify ACL2s system books\" under the ACL2s menu and retry after successful recertification.") (value :invisible))
(include-book "custom" :dir :acl2s-modes :uncertified-okp nil :ttags :all)

#+acl2s-startup (er-progn (assign fmt-error-msg "Problem setting up ACL2s mode.") (value :invisible))

;Settings common to all ACL2s modes
(acl2s-common-settings)
;(acl2::xdoc acl2s::defunc) ;; 3 seconds is too much time to spare -- commenting out [2015-02-01 Sun]

; Non-events:
;(set-guard-checking :none)

(acl2::in-package "ACL2S")

; ******************* END INITIALIZATION FOR ACL2s MODE ******************* ;
;$ACL2s-SMode$;ACL2s
(in-package "ACL2")
:pe and
:pe and-macro
:trans (+ (- 27 x) 21 13)

:trans 'x
:trans (quote x)

(defmacro doublequote (x)
  `(quote (quote ,x)))

:trans (doublequote 123)
:trans (doublequote foo)
(doublequote foo)

(defun expand-and (list)
  (if (consp list)
    (if (consp (rest list))
      `(if ,(first list)
         ,(expand-and (rest list))
         nil)
      (first list))
    t))

(defmacro my-and (&rest args)
  (expand-and args))

:trans (my-and)
:trans (my-and x)
:trans (my-and x y)
:trans (my-and x y z)

(thm (implies (and (natp m) (natp n))
              (iff (< m n) (o< m n))))

(thm (implies (natp n) (o-p n)))

(defun swap-tree (x)
  (if (atom x)
    x
    (cons (swap-tree (cdr x))
          (swap-tree (car x)))))

(swap-tree '((1 2 . 3) 4 . 5))
(swap-tree '((1 . (2 . 3)) . (4 . 5)))

(defun flatten-tree (x)
  (cond ((atom x) (list x))
        (t (append (flatten-tree (car x))
                   (flatten-tree (cdr x))))))


(set-termination-method :measure)
(set-gag-mode nil)


(defun sum-squares (n)
  (declare (xargs :termination-method :measure
                  :measure (if (natp n) n 0)))
  (if (zp n)
    0
    (+ (* n n) (sum-squares (- n 1)))))

(defthm sum-squares-closed-form
  (implies (natp n)
           (equal (sum-squares n)
                  (/ (* n (+ n 1) (+ (* 2 n) 1)) 6))))

(defun f (n)
  (/ (* n (+ n 1) (+ (* 2 n) 1)) 6))

(thm (iff (implies (not (zp n))
                   (implies (equal (sum-squares (- n 1)) (f (- n 1)))
                            (equal (sum-squares n) (f n))))
          (implies (and (not (zp n))
                   (equal (sum-squares (- n 1)) (f (- n 1))))
              (equal (sum-squares n) (f n)))))#|ACL2s-ToDo-Line|#


