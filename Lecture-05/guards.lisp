(in-package "ACL2")

(defun fact (n)
  (if (zp n)
      1
      (* n (fact (1- n)))))

(defun factt (n)
  (declare (type (signed-byte 16) n)
           (optimize speed (safety 0)))
  (if (zp n)
      1
      (* n (factt (1- n)))))

