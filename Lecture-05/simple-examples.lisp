; ****************** BEGIN INITIALIZATION FOR ACL2s MODE ****************** ;
; (Nothing to see here!  Your actual file is after this initialization code);

#+acl2s-startup (er-progn (assign fmt-error-msg "Problem loading the CCG book.~%Please choose \"Recertify ACL2s system books\" under the ACL2s menu and retry after successful recertification.") (value :invisible))
(include-book "ccg/ccg" :uncertified-okp nil :dir :acl2s-modes :ttags ((:ccg)) :load-compiled-file nil);v4.0 change

;Common base theory for all modes.
#+acl2s-startup (er-progn (assign fmt-error-msg "Problem loading ACL2s base theory book.~%Please choose \"Recertify ACL2s system books\" under the ACL2s menu and retry after successful recertification.") (value :invisible))
(include-book "base-theory" :dir :acl2s-modes)


#+acl2s-startup (er-progn (assign fmt-error-msg "Problem loading ACL2s customizations book.~%Please choose \"Recertify ACL2s system books\" under the ACL2s menu and retry after successful recertification.") (value :invisible))
(include-book "custom" :dir :acl2s-modes :uncertified-okp nil :ttags :all)

#+acl2s-startup (er-progn (assign fmt-error-msg "Problem setting up ACL2s mode.") (value :invisible))

;Settings common to all ACL2s modes
(acl2s-common-settings)
;(acl2::xdoc acl2s::defunc) ;; 3 seconds is too much time to spare -- commenting out [2015-02-01 Sun]

; Non-events:
;(set-guard-checking :none)

(acl2::in-package "ACL2S")

; ******************* END INITIALIZATION FOR ACL2s MODE ******************* ;
;$ACL2s-SMode$;ACL2s
(in-package "ACL2")

(defun app (lhs rhs)
  (if (endp lhs)
    rhs
    (cons (first lhs) (app (rest lhs) rhs))))

(defthm consp-app
  (iff (consp (app lhs rhs))
       (or (consp lhs) (consp rhs))))

#+(or)
(defthm app-associative-special-case
  (equal (app (app a a) a)
         (app a (app a a))))

(set-gag-mode nil)

#+(or)
(defthm app-associative-special-case
  (equal (app (app a a) a)
         (app a (app a a))))

(set-gag-mode t)

#+(or)
(defthm app-associative-special-case
  (equal (app (app a a) a)
         (app a (app a a))))

(set-gag-mode :goals)

(set-gag-mode nil)

(defthm app-associative
  (equal (app (app a b) c)
         (app a (app b c))))

#+(or)
(defthm app-associative-special-case
  (equal (app (app a a) a)
         (app a (app a a))))

(defthm app-associative-special-case
  (equal (app (app a a) a)
         (app a (app a a)))
  :rule-classes nil)#|ACL2s-ToDo-Line|#


(defun my-rev (list)
  (if (endp list)
    '()
    (app (my-rev (rest list)) (list (first list)))))

#+(or)
(defthm my-rev-rev
  (equal (my-rev (my-rev xs)) xs))

(defthm my-rev-rev
  (implies (true-listp xs)
           (equal (my-rev (my-rev xs)) xs)))

#+(or)
(defthm alt-rev
  (equal (my-rev xs)
         (if (endp xs)
           nil
           (if (endp (rest xs))
             (list (first xs))
             (cons (first (my-rev (rest xs)))
                   (my-rev (cons (first xs)
                                 (my-rev (rest (my-rev (rest xs))))))))))
  :rule-classes nil)

(defthm consp-my-rev
  (equal (consp (my-rev xs))
         (consp xs)))

#+(or)
(defthm alt-rev
  (equal (my-rev xs)
         (if (endp xs)
           nil
           (if (endp (rest xs))
             (list (first xs))
             (cons (first (my-rev (rest xs)))
                   (my-rev (cons (first xs)
                                 (my-rev (rest (my-rev (rest xs))))))))))
  :rule-classes nil)

(defthm true-listp-my-rev
  (true-listp (my-rev xs)))

(defthm alt-rev
  (equal (my-rev xs)
         (if (endp xs)
           nil
           (if (endp (rest xs))
             (list (first xs))
             (cons (first (my-rev (rest xs)))
                   (my-rev (cons (first xs)
                                 (my-rev (rest (my-rev (rest xs))))))))))
  :rule-classes nil)

#+(or)
(defun no-util-rev (xs)
  (if (endp xs)
    nil
    (if (endp (rest xs))
      (list (first xs))
      (let ((rev-rest (no-util-rev (rest xs))))
        (cons (first rev-rest)
              (no-util-rev (cons (first xs)
                                 (no-util-rev (rest rev-rest)))))))))


