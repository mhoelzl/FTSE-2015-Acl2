; ****************** BEGIN INITIALIZATION FOR ACL2s MODE ****************** ;
; (Nothing to see here!  Your actual file is after this initialization code);

#+acl2s-startup (er-progn (assign fmt-error-msg "Problem loading the CCG book.~%Please choose \"Recertify ACL2s system books\" under the ACL2s menu and retry after successful recertification.") (value :invisible))
(include-book "ccg/ccg" :uncertified-okp nil :dir :acl2s-modes :ttags ((:ccg)) :load-compiled-file nil);v4.0 change

;Common base theory for all modes.
#+acl2s-startup (er-progn (assign fmt-error-msg "Problem loading ACL2s base theory book.~%Please choose \"Recertify ACL2s system books\" under the ACL2s menu and retry after successful recertification.") (value :invisible))
(include-book "base-theory" :dir :acl2s-modes)


#+acl2s-startup (er-progn (assign fmt-error-msg "Problem loading ACL2s customizations book.~%Please choose \"Recertify ACL2s system books\" under the ACL2s menu and retry after successful recertification.") (value :invisible))
(include-book "custom" :dir :acl2s-modes :uncertified-okp nil :ttags :all)

#+acl2s-startup (er-progn (assign fmt-error-msg "Problem setting up ACL2s mode.") (value :invisible))

;Settings common to all ACL2s modes
(acl2s-common-settings)
;(acl2::xdoc acl2s::defunc) ;; 3 seconds is too much time to spare -- commenting out [2015-02-01 Sun]

; Non-events:
;(set-guard-checking :none)

(acl2::in-package "ACL2S")

; ******************* END INITIALIZATION FOR ACL2s MODE ******************* ;
;$ACL2s-SMode$;ACL2s
(in-package "ACL2")

(defun debug-output (n)
  (cw "This is some debug output: ~x0~%" n))

(debug-output 123)
(debug-output "Some string value")
(debug-output '(this is (a (nested)
                           list 
                           (of symbols)
                           (and (other stuff)))))

(defun string-output (str)
  (cw "This is a string: ~x0, and also: ~@0~%" str))

(string-output "foo")
#+(or)
(string-output 'a)

#+(or)
(defun output-and-value (arg)
  (cw "Some debug output: ~x0~%" arg)
  (null arg))

(defun output-and-value (arg)
  (prog2$
   (cw "Some debug output: ~x0~%" arg)
   (null arg)))

(output-and-value 123)
(output-and-value nil)

(if (output-and-value nil)
  (output-and-value 1)
  (output-and-value 2))

(if (output-and-value t)
  (output-and-value 1)
  (output-and-value 2))

(defun conditional-output (arg)
  (prog2$
   #+ccl
   (cw "Running on CCL!~%")
   #+sbcl
   (cw "Running on SBCL!~%")
   (cw "The string was ~x0~%" arg)))

(conditional-output "My string")

#+(or)
(defun fact-iter (n acc)
  (if (equal n 0)
    acc
    (fact-iter (- n 1) (* acc n))))

(defun fact-iter (n acc)
  (if (zp n)
    acc
    (fact-iter (- n 1) (* acc n))))

(trace$ fact-iter)

(fact-iter 10 1)
(untrace$ fact-iter)
(fact-iter 10 1)

(defun my-rev-1 (list)
  (if (endp list)
    '()
    (append (my-rev-1 (rest list)) (list (first list)))))

(my-rev-1 '())
(my-rev-1 '(1 2 3))
#+(or)
(my-rev-1 'a)

(defun my-rev-2 (list)
  (if (atom list)
    '()
    (append (my-rev-2 (rest list)) (list (first list)))))

(my-rev-2 '())
(my-rev-2 '(1 2 3))
(my-rev-2 'a)
(my-rev-2 '(1 2 3 . 4))
#+(or)
(my-rev-1 '(1 2 3 . 4))

(pe 'atom)
:pe atom
(pe 'endp)

(defun id1 (x)
  (declare (xargs :guard t))
  x)

(thm (equal (consp (id1 x)) (consp x)))
(thm (equal (natp (id1 x)) (natp x))) 

(consp (id1 '(a b c)))
(natp (id1 123))
(consp (id1 'a))

(defun id2 (x)
  (declare (xargs :guard (true-listp x)))
  x)

(thm (equal (consp (id2 x)) (consp x)))
(thm (equal (natp (id2 x)) (natp x)))

(consp (id2 '(a b c)))
#+(or)
(natp (id2 123))
#+(or)
(consp (id2 'a))

(defun call-id1 (x)
  (declare (xargs :guard t))
  (id1 x))

#+(or)
(defun call-id2 (x)
  (declare (xargs :guard t))
  (id2 x))

(defun call-id2 (x)
  (declare (xargs :guard (true-listp x)))
  (id2 x))

#+(or)
(defun my-rev-3 (list)
  (declare (xargs :guard t))
  (if (endp list)
    '()
    (append (my-rev-3 (rest list)) (list (first list)))))

#+(or)
(defun my-rev-3 (list)
  (declare (xargs :guard (listp list)))
  (if (endp list)
    '()
    (append (my-rev-3 (rest list)) (list (first list)))))

(defun my-rev-3 (list)
  (declare (xargs :guard (true-listp list)))
  (if (endp list)
    '()
    (append (my-rev-3 (rest list)) (list (first list)))))

(defun 123-listp (x)
  (declare (xargs :guard t))
  (if (atom x)
    (null x)
    (and (let ((n (first x)))
           (and (integerp x)
                (or (= n 1) (= n 2) (= n 3))))
         (123-listp (rest x)))))

(pe '=)
(123-listp '(1 2 3))
(123-listp '(1 2 3 4))
(123-listp '(5))
(123-listp 1)
(123-listp '())
(123-listp nil)

(defun my-rev-4 (list)
  (declare (xargs :guard (123-listp list)))
  (if (endp list)
    '()
    (append (my-rev-4 (rest list)) (list (first list)))))#|ACL2s-ToDo-Line|#


