(in-package "ACL2")

(defun tree-leafp (x)
  (equal x '%%tree-leaf%%))

(defun tree-leaf ()
  '%%tree-leaf%%)

;;; Trees of numbers
(defun nodep (x)
  (and (consp x)
       (equal (first x) '%%binary-tree-node%%)
       (acl2-numberp (second x))
       (or (tree-leafp (third x)) (nodep (third x)))
       (or (tree-leafp (fourth x)) (nodep (fourth x)))
       (null (cddddr x))))

(defun node (val left right)
  (if (and (or (tree-leafp left) (nodep left))
           (or (tree-leafp right) (nodep right)))
      (list '%%binary-tree-node%% (fix val) left right)
      nil))

(set-gag-mode nil)

(defun treep (x)
  (or (leafp x)
      (nodep x)))

(defthm nodes-are-treep
  (treep (node val left right))
  :rule-classes nil)

(defun val (tree)
  (second tree))

(defun left (tree)
  (third tree))

(defun right (tree)
  (fourth tree))

(defthm tree-node-extensionality
  (implies (and (nodep x) (nodep y)
                (equal (val x) (val y))
                (equal (left x) (left y))
                (equal (right x) (right y)))
           (equal x y))
  :rule-classes nil)

;;; Induction scheme derived from lists

(defun size (tree)
  (if (not (nodep tree))
      0
      (+ (size (left tree)) (size (right tree)) 1)))

(defun every<= (tree x)
  "Is every element of TREE <= X?"
  (if (not (nodep tree))
      t
      (and (<= (val tree) x)
           (every<= (left tree) x)
           (every<= (right tree) x))))

(defun every>= (tree x)
  "Is every element of TREE >= X?"
  (if (not (nodep tree))
      t
      (and (>= (val tree) x)
           (every>= (left tree) x)
           (every>= (right tree) x))))

(defun tree-orderedp (tree)
  (if (nodep tree)
      (and (every<= (left tree) (val tree))
           (every>= (right tree) (val tree))
           (tree-orderedp (left tree))
           (tree-orderedp (right tree)))
      t))


(defun insert (x tree)
  (if (not (nodep tree))
      (node x (tree-leaf) (tree-leaf))
      (if (<= x (val tree))
          (node (val tree) (insert x (left tree)) (right tree))
          (node (val tree) (left tree) (insert x (right tree))))))

(defthm insert-maintains-treep
  (implies (treep tree)
           (treep (insert x tree)))
  :rule-classes nil)

(defthm insert-maintains-order/1
  (implies (and (every<= tree y)
                (<= x y))
           (every<= (insert x tree) y)))

(defthm insert-maintains-order/2
  (implies (and (every>= tree y)
                (>= x y))
           (every>= (insert x tree) y)))

(defthm insert-maintains-order
    (implies (tree-orderedp tree)
             (tree-orderedp (insert x tree))))

(defun tree-search (x tree)
  (if (not (nodep tree))
    nil
    (or (equal x (val tree))
        (tree-search x (left tree))
        (tree-search x (right tree)))))

(defun binary-search (x tree)
  (if (not (nodep tree))
      nil
      (let ((y (val tree)))
        (cond ((equal x y) t)
              ((<= x y) (binary-search x (left tree)))
              (t (binary-search x (right tree)))))))

(defthm binary-search-correct-for-ordered-trees/lemma
  (implies (and (treep tree)
                (tree-orderedp tree))
           (equal (tree-search x tree)
                  (binary-search x tree))))

(defthm binary-search-correct-for-ordered-trees
  (implies (and (treep tree)
                (tree-orderedp tree))
           (equal (binary-search x tree)
                  (tree-search x tree))))

(in-theory (disable binary-search-correct-for-ordered-trees/lemma))

#+(or)
(defthm tree-search-insert/1
  (implies (and (acl2-numberp x) (acl2-numberp y) (treep tree))
           (implies (tree-search x (insert y tree))
                    (or (= x y)
                        (tree-search x tree))))
  :rule-classes nil)

(defthm tree-search-insert/2
    (implies (and (acl2-numberp x) (acl2-numberp y) (treep tree)
                  (not (= x y)))
           (implies (tree-search x (insert y tree))
                    (tree-search x tree))))

(defthm tree-search-insert/3
    (implies (and (acl2-numberp x) (acl2-numberp y) (treep tree)
                  (not (= x y)))
             (iff (tree-search x (insert y tree))
                  (tree-search x tree))))

(defthm tree-search-insert/4
    (implies (and (acl2-numberp x) (acl2-numberp y) (treep tree)
                  (= x y))
             (tree-search x (insert y tree))))

(defthm tree-search-insert
    (implies (and (acl2-numberp x) (acl2-numberp y) (treep tree))
             (iff (tree-search x (insert y tree))
                  (or (= x y) (tree-search x tree)))))#|ACL2s-ToDo-Line|#


