; ****************** BEGIN INITIALIZATION FOR ACL2s MODE ****************** ;
; (Nothing to see here!  Your actual file is after this initialization code);

#+acl2s-startup (er-progn (assign fmt-error-msg "Problem loading the CCG book.~%Please choose \"Recertify ACL2s system books\" under the ACL2s menu and retry after successful recertification.") (value :invisible))
(include-book "ccg/ccg" :uncertified-okp nil :dir :acl2s-modes :ttags ((:ccg)) :load-compiled-file nil);v4.0 change

;Common base theory for all modes.
#+acl2s-startup (er-progn (assign fmt-error-msg "Problem loading ACL2s base theory book.~%Please choose \"Recertify ACL2s system books\" under the ACL2s menu and retry after successful recertification.") (value :invisible))
(include-book "base-theory" :dir :acl2s-modes)


#+acl2s-startup (er-progn (assign fmt-error-msg "Problem loading ACL2s customizations book.~%Please choose \"Recertify ACL2s system books\" under the ACL2s menu and retry after successful recertification.") (value :invisible))
(include-book "custom" :dir :acl2s-modes :uncertified-okp nil :ttags :all)

#+acl2s-startup (er-progn (assign fmt-error-msg "Problem setting up ACL2s mode.") (value :invisible))

;Settings common to all ACL2s modes
(acl2s-common-settings)
;(acl2::xdoc acl2s::defunc) ;; 3 seconds is too much time to spare -- commenting out [2015-02-01 Sun]

; Non-events:
;(set-guard-checking :none)

(acl2::in-package "ACL2S")

; ******************* END INITIALIZATION FOR ACL2s MODE ******************* ;
;$ACL2s-SMode$;ACL2s
(in-package "ACL2")

;;; ============================================================
;;; Material from Previous Lecture
;;; ============================================================

;;; Utilities
;;; =========

(defun set-union (xs ys)
  "Compute the union of lists xs and ys regarded as sets."
  (declare (xargs :guard (and (true-listp xs)
                              (true-listp ys))))
  (if (endp xs)
    ys
    (if (member-equal (first xs) ys)
      (set-union (rest xs) ys)
      (cons (first xs) (set-union (rest xs) ys)))))

(defun set-subset (xs ys)
  "Check whether xs is a subset of ys."
  (declare (xargs :guard (and (true-listp xs) (true-listp ys))))
  (if (endp xs)
    t
    (and (member-equal (first xs) ys)
         (set-subset (rest xs) ys))))

;;; Syntax of Propositional Logic
;;; =============================

;;; The available operators
(defconst *operators* '(or and implies iff xor))

(defun varp (exp)
  (declare (xargs :guard t))
  (and (symbolp exp)
       (not (member exp '(t nil)))))

(defun exprp (exp)
  "Check whether exp is an expression."
  (declare (xargs :guard t))
  (cond ((atom exp)
         (symbolp exp))
        ((and (true-listp exp) (equal (length exp) 2))
         (and (equal (first exp) 'not)
              (exprp (second exp))))
        ((and (true-listp exp) (equal (length exp) 3))
         (and (member (first exp) *operators*)
              (exprp (second exp))
              (exprp (third exp))))
        (t nil)))


;;; Interpretation as Booleans
;;; ===========================

(defun lookup (var env)
  "Look up var in env and return the result as Boolean.
Returns nil if no binding for var is contained in env."
  (declare (type symbol var) (type (satisfies alistp) env))
  (and (cdr (assoc var env)) t))

(defun eval-bool (exp env)
  "Evaluate exp as Boolean value in environment env"
  (declare (xargs :guard (and (exprp exp) (alistp env))))
  (cond ((symbolp exp)
         (if (or (equal exp t) (equal exp nil))
           exp
           (lookup exp env)))
        ((atom exp) nil)
        ((equal (length exp) 2)
         (not (eval-bool (second exp) env)))
        ((equal (length exp) 3)
         (let ((lhs (eval-bool (second exp) env))
               (rhs (eval-bool (third exp) env)))
           (case (first exp)
                 (or        (or      lhs rhs))
                 (and       (and     lhs rhs))
                 (implies   (implies lhs rhs))
                 (iff       (iff     lhs rhs))
                 (xor       (xor     lhs rhs))
                 (otherwise nil))))
        (t nil)))


;;; Substitutions
;;; =============

(defun substitute-1 (new old exp)
  (declare (xargs :guard t))
  (cond ((equal old exp) new)
        ((or (atom exp) (not (true-listp exp))) exp)
        ((equal (length exp) 2)
         (list (first exp)
               (substitute-1 new old (second exp))))
        ((equal (length exp) 3)
         (list (first exp)
               (substitute-1 new old (second exp))
               (substitute-1 new old (third exp))))
        (t exp)))

(substitute-1 '(and a b)
              '(and b a)
              '(implies (and b a)
                        (or (and b a) (and c a))))

(defthm substitute-1-identity
  (equal (substitute-1 exp1 exp1 exp2)
         exp2))


(defthm substitute-equivalent-expressions
  (implies (equal (eval-bool exp1 env) (eval-bool exp2 env))
           (equal (eval-bool (substitute-1 exp1 exp2 exp3) env)
                  (eval-bool exp3 env))))

(defun substitute-all (new-old-pairs exp)
  (declare (xargs :guard (alistp new-old-pairs)))
  (let ((new-old-pair (assoc exp new-old-pairs :test 'equal)))
    (cond (new-old-pair
           (cdr new-old-pair))
          ((or (atom exp) (not (true-listp exp))) exp)
          ((equal (length exp) 2)
           (list (first exp)
                 (substitute-all new-old-pairs (second exp))))
          ((equal (length exp) 3)
           (list (first exp)
                 (substitute-all new-old-pairs (second exp))
                 (substitute-all new-old-pairs (third exp))))
          (t exp))))

;;; Semantic Truth
;;; ==============

(defun alist-listp (list)
  (declare (xargs :guard t))
  (if (atom list)
    (not list)
    (and (alistp (first list))
         (alist-listp (rest list)))))

(defun extend-envs (var value envs)
  (declare (xargs :guard (and (symbolp var) (alist-listp envs))))
  (if (endp envs)
    '()
    (cons (cons (cons var value) (first envs))
          (extend-envs var value (rest envs)))))

(defthm alist-listp-append
  (implies (and (alist-listp xs) (alist-listp ys))
           (alist-listp (append xs ys))))

(defthm extend-envs-alist-listp
  (implies (alist-listp envs)
           (alist-listp (extend-envs var value envs))))

(defun generate-envs (vars)
  (declare (xargs :guard (symbol-listp vars)))
  (if (endp vars)
    '(())
    (let ((rest-envs (generate-envs (rest vars))))
      (append (extend-envs (first vars) t rest-envs)
              (extend-envs (first vars) nil rest-envs)))))

(defthm generate-envs-is-alist-listp
  (alist-listp (generate-envs vars)))

(defun vars (exp)
  "Compute all (propositional) variables occurring in exp."
  (declare (xargs :guard (exprp exp)))
  (cond ((atom exp)
         (if (varp exp)
           (list exp)
           '()))
        ((equal (length exp) 2)
         (if (equal (first exp) 'not)
           (vars (second exp))
           '()))
        ((equal (length exp) 3)
         (if (member (first exp) *operators*)
           (set-union (vars (second exp)) (vars (third exp)))
           '()))
        (t '())))

(defun any-true (list)
  (declare (xargs :guard (true-listp list)))
  (if (endp list)
    nil
    (or (and (first list) t)
        (any-true (rest list)))))

(defun all-true (list)
  (declare (xargs :guard (true-listp list)))
  (if (endp list)
    t
    (and (first list)
         (all-true (rest list)))))

(defun model-check-aux (term envs)
  (declare (xargs :guard (and (exprp term) (alist-listp envs))))
  (if (endp envs)
    '()
    (cons (eval-bool term (first envs))
          (model-check-aux term (rest envs)))))

(defun model-check (term)
  (declare (xargs :guard (exprp term)))
  (let ((results (model-check-aux term (generate-envs (vars term)))))
    (cond ((all-true results)
           :valid)
          ((any-true results)
           :satisfiable)
          (t
           :insatisfiable))))


;;; Desugaring Terms
;;; ================

(defun desugar (exp)
  (declare (xargs :guard (exprp exp)))
  (cond ((atom exp)
         (if (symbolp exp)
           exp
           nil))
        ((and (true-listp exp) (equal (length exp) 2))
         (if (equal (first exp) 'not)
           `(not ,(desugar (second exp)))
           nil))
        ((and (true-listp exp) (equal (length exp) 3))
         (if (member (first exp) *operators*)
           (let ((lhs (desugar (second exp)))
                 (rhs (desugar (third exp))))
             (case (first exp)
                   (or       `(or ,lhs ,rhs))
                   (and      `(not (or (not ,lhs) (not ,rhs))))
                   (implies  `(or (not ,lhs) ,rhs))
                   (iff      `(or (not (or (not ,lhs) (not ,rhs)))
                                  (not (or ,lhs ,rhs))))
                   (xor      `(or (not (or ,lhs (not ,rhs)))
                                  (not (or (not ,lhs) ,rhs))))
                   (otherwise nil)))
           nil))
        (t nil)))

(thm (and (equal (desugar '(implies (and a b) c))
                 '(or (not (not (or (not a) (not b)))) c)) 
          (equal (desugar '(and (implies a b) (xor (iff a b) c)))
                 '(not (or (not (or (not a) b))
                           (not (or (not (or (or (not (or (not a) (not b)))
                                                 (not (or a b)))
                                             (not c)))
                                    (not (or (not (or (not (or (not a) (not b)))
                                                      (not (or a b))))
                                             c)))))))))

(defthm desugaring-preserves-expressions
  (implies (exprp exp)
           (exprp (desugar exp))))
  
(defthm desugaring-preserves-meaning
  (implies (exprp exp)
           (equal (eval-bool exp env) (eval-bool (desugar exp) env))))

;;; ============================================================
;;; New Material Starts Here
;;; ============================================================

(defun desugared-exprp (exp)
  (declare (xargs :guard t))
  (cond ((atom exp)
         (symbolp exp))
        ((and (true-listp exp) (equal (length exp) 2))
         (and (equal (first exp) 'not)
              (desugared-exprp (second exp))))
        ((and (true-listp exp) (equal (length exp) 3))
         (and (equal (first exp) 'or)
              (desugared-exprp (second exp))
              (desugared-exprp (third exp))))
        (t nil)))

(thm (and (desugared-exprp (desugar '(implies (and a b) c)))
          (desugared-exprp (desugar '(and (implies a b) (xor (iff a b) c))))))

(defthmd desugaring-produces-desugared-expr
  (implies (exprp exp)
           (desugared-exprp (desugar exp))))

;;; Proof System
;;; ============

;;; We define (syntactic) proof item as a list of length 2 containing an
;;; expression as first element and a justification as second element.

(defun proof-item-exp (proof-item)
  (first proof-item))

(defun proof-item-justification (proof-item)
  (second proof-item))

(defun proof-itemp (index proof)
  (and (natp index) (true-listp proof)
       (< index (len proof))
       (let ((item (nth index proof)))
         (if (and (true-listp item) (equal (len item) 2))
           (let ((exp (proof-item-exp item))
                 (justification (proof-item-justification item)))
             (case (first justification)
                   ;; Axiom: Anything goes
                   (axiom t)
                   ;; Tertium non datur: |- (not phi) or phi
                   (tertium-non-datur
                    (let ((phi (second justification)))
                      (and (desugared-exprp exp)
                           (equal exp `(or (not ,phi) ,phi)))))
                   ;; Expansion: psi |- phi or psi
                   (expansion
                    (let* ((psi (second justification))
                           (j-index (third justification)))
                      (and (desugared-exprp exp)
                           (natp j-index)
                           (< j-index index)
                           (equal (proof-item-exp (nth j-index proof))
                                  psi)
                           (equal (first exp) 'or)
                           (equal (third exp) psi))))
                   ;; Contraction: phi or phi |- phi
                   (contraction
                    (let* ((j-index (second justification)))
                      (and (desugared-exprp exp)
                           (natp j-index)
                           (< j-index index)
                           (equal (proof-item-exp (nth j-index proof))
                                  `(or ,exp ,exp)))))
                   ;;; Associativity phi or (psi or xi) |- (phi or psi) or xi
                   (associativity
                    (let* ((phi (second justification))
                           (psi (third justification))
                           (xi (fourth justification))
                           (j-index (fifth justification)))
                      (and (desugared-exprp `(or (or ,phi ,psi) xi))
                           (natp j-index)
                           (< j-index index)
                           (equal (proof-item-exp (nth j-index proof))
                                  `(or ,phi (or ,psi ,xi)))
                           (equal exp `(or (or ,phi ,psi) ,xi)))))
                   ;;; Cut: (phi or psi) and (not phi or xi) |- psi or xi
                   (cut
                    (let ((phi (second justification))
                          (psi (third justification))
                          (xi (fourth justification))
                          (j-index-1 (fifth justification))
                          (j-index-2 (sixth justification)))
                      (and (desugared-exprp `(or ,phi ,psi))
                           (desugared-exprp `(or (not ,phi) ,xi))
                           (natp j-index-1)
                           (< j-index-1 index)
                           (natp j-index-2)
                           (< j-index-2 index)
                           (equal (proof-item-exp (nth j-index-1 proof))
                                  `(or ,phi ,psi))
                           (equal (proof-item-exp (nth j-index-2 proof))
                                  `(or (not ,phi) ,xi))
                           (equal exp `(or ,psi ,xi)))))
                   ;;; Instantiation: phi |- phi[v |-> psi]
                   (instantiate
                    (let ((phi (second justification))
                          (v (third justification))
                          (psi (fourth justification))
                          (j-index (fifth justification)))
                      (and (desugared-exprp phi)
                           (varp v)
                           (desugared-exprp psi)
                           (natp j-index)
                           (< j-index index)
                           (equal (proof-item-exp (nth j-index proof))
                                  phi)
                           (equal exp (substitute-1 psi v phi)))))
                   (otherwise nil)))
           nil))))

(defun proofp-aux (index proof)
  (declare (xargs :termination-method :measure
                  :measure (acl2-count (- (len proof) index))))
  (if (or (not (natp index))
          (<= (len proof) index))
    t
    (and (proof-itemp index proof)
         (proofp-aux (1+ index) proof))))

#+(or)
(defthmd proofp-aux-append
  (implies (and (proofp-aux 0 proof)
                (proof-itemp (len proof) (append proof item)))
           (proofp-aux 0 (append proof item))))

(defun proofp (proof)
  (if (atom proof)
    (not proof)
    (and (true-listp proof)
         (proofp-aux 0 proof))))

(defthmd proofp-is-true-listp
  (implies (proofp exp)
           (true-listp exp)))

(defun extend-proof (proof new-item)
  (append proof (list new-item)))

(defun tertium-non-datur (proof phi)
  (extend-proof proof
                (list `(or (not ,phi) ,phi)
                      `(tertium-non-datur ,phi))))

(defthmd tertium-non-datur-preserves-proof-itemp
  (implies (and (proofp proof) (desugared-exprp phi))
           (proof-itemp (len proof) (tertium-non-datur proof phi)))
  :hints (("Goal" :do-not-induct t)))

#+(or)
(defthm tertium-non-datur-preserves-proofp
  (implies (and (proofp proof)
                (proof-itemp (len proof) (tertium-non-datur proof phi)))
           (proofp (tertium-non-datur proof phi))))

(defun expand (proof phi j-index)
  (let ((psi (proof-item-exp (nth j-index proof))))
    (extend-proof proof
                  (list `(or ,phi ,psi)
                        `(expansion ,psi ,j-index)))))

(defthmd expand-preserves-proof-itemp
  (implies (and (proofp proof)
                (desugared-exprp `(or ,phi ,psi))
                (natp j-index) (< j-index (len proof))
                (equal (proof-item-exp (nth j-index proof)) psi))
           (proof-itemp (len proof) (expand proof phi j-index)))
  :hints (("Goal" :do-not-induct t)))

(defun contract (proof j-index)
  (let ((phi (second (proof-item-exp (nth j-index proof)))))
    (extend-proof proof
                  (list phi
                        `(contraction ,j-index)))))

(defthmd contract-preserves-proof-itemp
  (implies (and (proofp proof)
                (desugared-exprp phi)
                (natp j-index) (< j-index (len proof))
                (equal (proof-item-exp (nth j-index proof))
                       `(or ,phi ,phi)))
           (proof-itemp (len proof) (contract proof j-index)))
  :hints (("Goal" :do-not-induct t)))

(defun associativity (proof j-index)
  (let* ((exp (proof-item-exp (nth j-index proof)))
         (phi (second exp))
         (psi-or-xi (third exp))
         (psi (second psi-or-xi))
         (xi (third psi-or-xi)))
    (extend-proof proof
                  (list `(or (or ,phi ,psi) ,xi)
                        `(associativity ,phi ,psi ,xi ,j-index)))))

(defthmd associativity-preserves-proof-itemp
  (implies (and (proofp proof)
                (desugared-exprp `(or (or ,phi ,psi) ,xi))
                (natp j-index) (< j-index (len proof))
                (equal (proof-item-exp (nth j-index proof))
                       `(or ,phi (or ,psi ,xi))))
           (proof-itemp (len proof) (associativity proof j-index)))
  :hints (("Goal" :do-not-induct t)))

(defun cut (proof j-index-1 j-index-2)
  (let* ((exp1 (proof-item-exp (nth j-index-1 proof)))
         (exp2 (proof-item-exp (nth j-index-2 proof)))
         (phi (second exp1))
         (psi (third exp1))
         (xi (third exp2)))
    (extend-proof proof
                  (list `(or ,psi ,xi)
                        `(cut ,phi ,psi ,xi ,j-index-1 ,j-index-2)))))

(defthmd cut-preserves-proof-itemp
  (implies (and (proofp proof)
                (desugared-exprp `(or ,phi ,psi))
                (desugared-exprp `(or (not ,phi) ,xi))
                (natp j-index-1) (< j-index-1 (len proof))
                (natp j-index-2) (< j-index-2 (len proof))
                (equal (proof-item-exp (nth j-index-1 proof))
                       `(or ,phi ,psi))
                (equal (proof-item-exp (nth j-index-2 proof))
                       `(or (not ,phi) ,xi)))
           (proof-itemp (len proof) (cut proof j-index-1 j-index-2)))
  :hints (("Goal" :do-not-induct t)))


(defun instantiate (proof psi v j-index)
  (let ((phi (proof-item-exp (nth j-index proof))))
    (extend-proof proof
                  (list (substitute-1 psi v phi) 
                        `(instantiate ,phi ,v ,psi ,j-index)))))

(defthmd instantiate-preserves-proof-itemp
  (implies (and (proofp proof)
                (desugared-exprp phi)
                (varp v)
                (desugared-exprp psi)
                (natp j-index) (< j-index (len proof))
                (equal (proof-item-exp (nth j-index proof))
                       phi))
           (proof-itemp (len proof) (instantiate proof psi v j-index)))
  :hints (("Goal" :do-not-induct t)))

(proofp
 (cut
  (cut
   (cut
    (cut
     (cut
      (tertium-non-datur
       (tertium-non-datur
        (tertium-non-datur 
         (tertium-non-datur '() 'a)
         '(not a))
        '(not (not a)))
       '(not (not (not a))))
      0 1)
     1 2)
    2 3)
   0 6)
  7 0))

(cut
 (cut
  (cut
   (cut
    (cut
     (tertium-non-datur
      (tertium-non-datur
       (tertium-non-datur 
        (tertium-non-datur '() 'a)
        '(not a))
       '(not (not a)))
      '(not (not (not a))))
     0 1)
    1 2)
   2 3)
  0 6)
 7 0)

(defun build-proof-steps (steps)
  (if (endp steps)
    '()
    (let* ((step (first steps))
           (op (first step))
           (args (rest step)))
      `(,op ,(build-proof-steps (rest steps)) ,@args))))

(build-proof-steps '((cut 7 0)
                     (cut 0 6)
                     (cut 2 3)
                     (cut 1 2)
                     (cut 0 1)
                     (tertium-non-datur '(not (not (not a))))
                     (tertium-non-datur '(not (not a)))
                     (tertium-non-datur '(not a))
                     (tertium-non-datur 'a)))

(build-proof-steps 
 (reverse '((tertium-non-datur 'a)
            (tertium-non-datur '(not a))
            (tertium-non-datur '(not (not a)))
            (tertium-non-datur '(not (not (not a))))
            (cut 0 1)
            (cut 1 2)
            (cut 2 3)
            (cut 0 6)
            (cut 7 0))))

(defmacro proof (&rest steps)
  (build-proof-steps (reverse steps)))

(trans '(proof (tertium-non-datur 'a)))
(trans '(proof (tertium-non-datur 'a)
               (tertium-non-datur '(not a))))

(trans '(proof (tertium-non-datur 'a)
               (tertium-non-datur '(not a))
               (tertium-non-datur '(not (not a)))
               (tertium-non-datur '(not (not (not a))))
               (cut 0 1)
               (cut 1 2)
               (cut 2 3)
               (cut 0 6)
               (cut 7 0)))

(proof (tertium-non-datur 'a)
       (tertium-non-datur '(not a))
       (tertium-non-datur '(not (not a)))
       (tertium-non-datur '(not (not (not a))))
       (cut 0 1)
       (cut 1 2)
       (cut 2 3)
       (cut 0 6)
       (cut 7 0))

(defmacro tnd (proof exp)
  `(tertium-non-datur ,proof ',exp))

(defconst *proof-1*
  (proof (tnd a)
         (tnd (not a))
         (tnd (not (not a)))
         (tnd (not (not (not a))))
         (cut 0 1)
         (cut 1 2)
         (cut 2 3)
         (cut 0 6)
         (cut 7 0)))

(proofp *proof-1*)

(defconst *phi* '(or (not a) (not (not a))))
(defconst *psi* '(or (not (not (not a))) a))
(defconst *xi*  `(or (not ,*phi*) (not ,*psi*)))
(defconst *alpha*  `(or (not ,*xi*) (not ,*phi*)))

(defconst *proof-2*
  (proof (tnd a)                                  ; 0
         (tnd (not a))                            ; 1
         (tnd (not (not a)))                      ; 2
         (tnd (not (not (not a))))                ; 3
         (cut 0 1)                                ; 4
         (cut 1 2)                                ; 5
         (cut 2 3)                                ; 6
         (cut 0 6)                                ; 7
         (cut 7 0)                                ; 8
         (tertium-non-datur *xi*)                 ; 9 
         (associativity 9)                        ; 10
         (tertium-non-datur *alpha*)              ; 11
         (cut 10 11)                              ; 12
         (expand *alpha* 8)                       ; 13
         (cut 13 11)                              ; 14
         (cut 14 12)                              ; 15
         (contract 15)                            ; 16
         (tertium-non-datur `(not ,*xi*))         ; 17
         (cut 16 17)                              ; 18
         (expand `(not ,*xi*) 5)                  ; 19
         (cut 19 17)                              ; 20
         (cut 20 18)                              ; 21
         (contract 21)))                          ; 22

(proofp *proof-2*)

(defun add-proof-steps-aux (proof steps)
  (if (endp steps)
    proof
    (let* ((step (first steps))
           (op (first step))
           (args (rest step)))
      `(,op ,(add-proof-steps-aux proof (rest steps)) ,@args))))

(defmacro add-proof-steps (proof &rest steps)
  (add-proof-steps-aux proof (reverse steps)))

(defun commute-or (proof index)
  (let* ((phi-or-psi (proof-item-exp (nth index proof)))
         (phi (second phi-or-psi))
         (start-index (length proof)))
    (add-proof-steps proof
                     (tertium-non-datur phi)                     ; start-index
                     (cut index start-index))))                  ; 1
(proof (tnd a)
       (commute-or 0))

(proofp '((a (axiom)) ((or (not a) b) (axiom))))

(defun modus-ponens (proof phi-index phi-implies-psi-index)
  (let* ((phi-implies-psi (proof-item-exp (nth phi-implies-psi-index proof)))
         (psi (third phi-implies-psi))
         (start-index (length proof)))
    (add-proof-steps proof
                     (expand psi phi-index)                        ; start-index
                     (commute-or start-index)                      ; 1,2
                     (cut (+ start-index 2) phi-implies-psi-index) ; 3
                     (contract (+ start-index 3)))))               ; 4

(add-proof-steps '((a (axiom))
                   ((or (not a) b) (axiom)))
                 (modus-ponens 0 1))

(proofp
 (add-proof-steps '((a (axiom))
                    ((or (not a) b) (axiom)))
                  (modus-ponens 0 1)))


(defun prove-and (proof phi-index psi-index)
  (let* ((phi (proof-item-exp (nth phi-index proof)))
         (psi (proof-item-exp(nth psi-index proof)))
         (xi `(or (not ,phi) (not ,psi)))
         (alpha `(or (not ,xi) (not ,phi)))
         (start-index (length proof)))
    (add-proof-steps proof
                     (tertium-non-datur xi)                      ; start-index (9)
                     (associativity start-index)                 ; 1
                     (tertium-non-datur alpha)                   ; 2
                     (cut (+ start-index 1) (+ start-index 2))   ; 3
                     (expand alpha psi-index)                    ; 4
                     (cut (+ start-index 4) (+ start-index 2))   ; 5
                     (cut (+ start-index 5) (+ start-index 3))   ; 6
                     (contract (+ start-index 6))                ; 7
                     (tertium-non-datur `(not ,xi))              ; 8
                     (cut (+ start-index 7) (+ start-index 8))   ; 9
                     (expand `(not ,xi) phi-index)               ; 10
                     (cut (+ start-index 10) (+ start-index 8))  ; 11
                     (cut (+ start-index 11) (+ start-index 9))  ; 12
                     (contract (+ start-index 12)))))            ; 13

(defconst *proof-3*
  (proof (tnd a)                                  ; 0
         (tnd (not a))                            ; 1
         (tnd (not (not a)))                      ; 2
         (tnd (not (not (not a))))                ; 3
         (cut 0 1)                                ; 4
         (cut 1 2)                                ; 5
         (cut 2 3)                                ; 6
         (cut 0 6)                                ; 7
         (cut 7 0)                                ; 8
         (prove-and 5 8)))

(proofp *proof-3*)

(equal *proof-2* *proof-3*)#|ACL2s-ToDo-Line|#


