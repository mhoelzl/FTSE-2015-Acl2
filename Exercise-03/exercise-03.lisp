; ****************** BEGIN INITIALIZATION FOR ACL2s MODE ****************** ;
; (Nothing to see here!  Your actual file is after this initialization code);

#+acl2s-startup (er-progn (assign fmt-error-msg "Problem loading the CCG book.~%Please choose \"Recertify ACL2s system books\" under the ACL2s menu and retry after successful recertification.") (value :invisible))
(include-book "ccg/ccg" :uncertified-okp nil :dir :acl2s-modes :ttags ((:ccg)) :load-compiled-file nil);v4.0 change

;Common base theory for all modes.
#+acl2s-startup (er-progn (assign fmt-error-msg "Problem loading ACL2s base theory book.~%Please choose \"Recertify ACL2s system books\" under the ACL2s menu and retry after successful recertification.") (value :invisible))
(include-book "base-theory" :dir :acl2s-modes)


#+acl2s-startup (er-progn (assign fmt-error-msg "Problem loading ACL2s customizations book.~%Please choose \"Recertify ACL2s system books\" under the ACL2s menu and retry after successful recertification.") (value :invisible))
(include-book "custom" :dir :acl2s-modes :uncertified-okp nil :ttags :all)

#+acl2s-startup (er-progn (assign fmt-error-msg "Problem setting up ACL2s mode.") (value :invisible))

;Settings common to all ACL2s modes
(acl2s-common-settings)
;(acl2::xdoc acl2s::defunc) ;; 3 seconds is too much time to spare -- commenting out [2015-02-01 Sun]

; Non-events:
;(set-guard-checking :none)

(acl2::in-package "ACL2S")

; ******************* END INITIALIZATION FOR ACL2s MODE ******************* ;
;$ACL2s-SMode$;ACL2s
(in-package "ACL2")

(defun flatten-tree (x)
  (declare (xargs :guard t))
  (cond ((atom x) (list x))
        (t (append (flatten-tree (car x))
                   (flatten-tree (cdr x))))))

(set-gag-mode nil)

(defthm flatten-tree-rotation
  (implies (and (consp x) (consp (car x)))
           (equal (flatten-tree x)
                  (flatten-tree (cons (caar x)
                                      (cons (cdar x) (cdr x))))))
  :rule-classes nil)

#+(or)
(defun flat (x)
  (cond ((atom x)
         (list x))
        ((atom (car x))
         (cons (car x) (flat (cdr x))))
        (t
         (flat (cons (caar x)
                     (cons (cadr x) (cdr x)))))))

(defun bad-flat (x)
  (declare (xargs :mode :program))
  (cond ((atom x)
         (list x))
        (t
         (bad-flat (cons (caar x)
                         (cons (cadr x) (cdr x)))))))


(defun bad-flat-n (x n)
  (cond ((zp n)
         x)
        ((atom x)
         (list x))
        (t
         (bad-flat-n (cons (caar x)
                           (cons (cadr x) (cdr x)))
                     (1- n)))))

(set-guard-checking :none)

(bad-flat-n '(1 . 2) 0)
(bad-flat-n '(1 . 2) 1)
(bad-flat-n '(1 . 2) 2)
(bad-flat-n '(1 . 2) 3)

(set-guard-checking t)

(defun rotate (x)
  (declare (xargs :guard t))
  (if (or (atom x) (atom (car x)))
    x
    (cons (caar x)
          (cons (cdar x) (cdr x)))))

(defthm rotate-reduces-count-of-car
  (implies (and (consp x) (consp (car x)))
           (< (acl2-count (car (rotate x)))
              (acl2-count (car x))))
  :rule-classes nil)

(defun rotate-count (x)
  (cond ((atom x) 1)
        (t (+ (* 2 (rotate-count (car x)))
              (rotate-count (cdr x))))))

(defthm rotate-count-decreases-for-rotation
  (implies (and (consp x) (consp (car x)))
           (< (rotate-count (rotate x))
              (rotate-count x)))
  :rule-classes nil)

(defun flat (x)
  (declare (xargs :termination-method :measure
                  :measure (rotate-count x)
                  :guard t))
  (cond ((atom x)
         (list x))
        ((atom (car x))
         (cons (car x) (flat (cdr x))))
        (t    
         (flat (rotate x)))))

(defthm flat-flattens
  (equal (flat x)
         (flatten-tree x)))

(defun flatten-acc (x acc)
  (declare (xargs :guard (true-listp acc)))
  (if (atom x)
    (cons x acc)
    (flatten-acc (car x) (flatten-acc (cdr x) acc))))

#+(or)
(defthm flatten-acc-flattens
  (equal (flatten-acc x '())
         (flatten-tree x)))

(defthm flatten-acc-flattens/1
  (equal (flatten-acc x xs)
         (append (flatten-tree x) xs)))

(defthm flatten-acc-flattens
  (equal (flatten-acc x '())
         (flatten-tree x))
  :rule-classes nil)

(defun mc-flat (x)
  (flatten-acc x '()))

(defthm mc-flat-flattens
  (equal (mc-flat x)
         (flatten-tree x)))

(trace$ flatten-tree flat flatten-acc)

(flatten-tree '(((1 . (2 . 3)) . ((4 . 5) . (6 . 7))) . 8))
(flat '(((1 . (2 . 3)) . ((4 . 5) . (6 . 7))) . 8))
(mc-flat '(((1 . (2 . 3)) . ((4 . 5) . (6 . 7))) . 8))

(untrace$ flatten-tree flat flatten-acc)

(defun build-tree (n)
  (declare (xargs :guard (natp n)))
  (if (zp n)
    t
    (let ((subtree (build-tree (1- n))))
      (cons subtree subtree))))

(defun number-of-leaves (x)
  (if (atom x)
    1
    (+ (number-of-leaves (car x))
       (number-of-leaves (cdr x)))))

(defthm build-tree-+1-doubles-number-of-leaves
  (implies (natp n)
           (equal (number-of-leaves (build-tree (1+ n)))
                  (* 2 (number-of-leaves (build-tree n))))))

(acl2s-defaults :set testing-enabled nil)

(defthm build-tree-n-has-2^n-leaves
  (implies (natp n)
           (equal (number-of-leaves (build-tree n))
                  (expt 2 n))))

(acl2s-defaults :set testing-enabled t)

(let ((tree (build-tree 15)))
  (time$ (prog2$ (flatten-tree tree) 'done)))
(let ((tree (build-tree 15)))
  (time$ (prog2$ (flat tree) 'done)))
(let ((tree (build-tree 15)))
  (time$ (prog2$ (mc-flat tree) 'done)))

(let ((tree (build-tree 20)))
  (time$ (prog2$ (flatten-tree tree) 'done)))
#+(or)
(let ((tree (build-tree 20)))
  (time$ (prog2$ (flat tree) 'done)))
(let ((tree (build-tree 20)))
  (time$ (prog2$ (mc-flat tree) 'done)))

(let ((tree (build-tree 24)))
  (time$ (prog2$ (flatten-tree tree) 'done)))
(let ((tree (build-tree 24)))
  (time$ (prog2$ (mc-flat tree) 'done)))

#+(or)
(let ((tree (build-tree 26)))
  (time$ (prog2$ (flatten-tree tree) 'done)))
#+(or)
(let ((tree (build-tree 26)))
  (time$ (prog2$ (mc-flat tree) 'done)))

#+(or)
(let ((tree (build-tree 28)))
  (time$ (prog2$ (mc-flat tree) 'done)))#|ACL2s-ToDo-Line|#
